#!/bin/bash
# write firmware to STM32 FLASH

source "options.sh"

cd "${BOARD}"

if [ "$FLASHER" == "openocd" ]
then
  if [ "$INTERFACE" == "jlink" ]
  then
    if [ "$MODE" == "jtag" ]
    then
      # J-link (JTAG)
      openocd ${OPENOCD_OPT} \
              -f "interface/jlink.cfg" \
              -f "target/${TARGET_CFG}" \
              ${OPENOCD_CMD} \
              -c "init" -c "reset init" \
              -c "flash write_image erase unlock ${IMAGE}" \
              -c "reset" -c "shutdown"
    else # if [ "$MODE" == "swd" ]
      # J-Link (SWD)
      openocd ${OPENOCD_OPT} \
              -f "interface/jlink.cfg" \
              -c "transport select swd" \
              -f "target/${TARGET_CFG}" \
              ${OPENOCD_CMD} \
              -c "init" -c "reset init" \
              -c "flash write_image erase unlock ${IMAGE}" \
              -c "reset" -c "shutdown"
    fi
  elif [ "$INTERFACE" == "stlink" ]
  then
    # ST-Link (SWD)
    openocd ${OPENOCD_OPT} \
            -f "interface/stlink-${STLINK_VER}.cfg" \
            -f "target/${TARGET_CFG}" \
            ${OPENOCD_CMD} \
            -c "init" -c "reset init" \
            -c "flash write_image erase unlock ${IMAGE}" \
            -c "reset" -c "shutdown"
  else # if [ "$INTERFACE" == "usb-blaster" ]
    # USB-blaster (JTAG)
    openocd ${OPENOCD_OPT} \
            -f "interface/altera-usb-blaster.cfg" \
            -c "transport select jtag" \
            -f "target/${TARGET_CFG}" \
            ${OPENOCD_CMD} \
            -c "init" -c "reset init" \
            -c "flash write_image erase unlock ${IMAGE}" \
            -c "reset" -c "shutdown"
  fi
elif [ "$FLASHER" == "black-magic" ]
then
  # Black Magic Probe (BMP) via GDB
  arm-none-eabi-gdb \
    -ex "target extended-remote /dev/ttyACM0" \
    -ex "mon swdp_scan" \
    -ex "att 1" \
    -ex "mon erase_mass" \
    -ex "load ${IMAGE}" \
    -ex "detach" \
    -ex "kill" \
    -ex "quit"
#    -ex "run" \
#    -ex "detach"
else # "$FLASHER" == "stlink"
  # ST-Link utility
  st-flash ${ST_FLASH_OPT} write build/${BOARD}.bin 0x08000000
fi

cd -

