Test project ("Hello world") for STM32 Discovery kit
====================================================
 - This demo project for my students.
 - Compiled under Debian Linux (Buster)
 - Used: STM32 CubeMX, GNU Make and arm-none-eabi-gcc/binutils

## Files:
 - "options.sh"   - build/FLASH options (select hardware)
 - "_make.sh"     - build firmware
 - "_flash.sh"    - flash firmware by ST-Link/J-Link
 - "_clean.sh"    - clean project directory
 - "disco_test.c" - main source file of project
 - "disco_test.h" - header of "disco_test.c" for run from "main.c"
 - "INSTALL.md"   - short help to install software (ARM-GCC/CubeMX)
 - "README.md"    - this short help

## Directories:
 - doc/            - documentation (schemes/datasheets/BOMs/pinouts)
 - lib/            - libs (BSP/FLASH/EEPROM/MicroRL/TFS)
 - discovery-l100/ - CubeMX directory tree for Discovery-L100 (STM32L100RCT6)
 - discovery-f100/ - CubeMX directory tree for Discovery-F100 (STM32F100RBT6B)

## Connect Discovery-L100 to USB-UART 3.3V LVTTL converter
| STM32L100RCT6 | Blue pill    | UART  | Comment (direction)   | Color   |
|:-------------:|:------------ |:----- |:--------------------- |:------- |
| PA10 (RX1)    | P2:23 (PA10) | TxD   | Data from PC to STM32 | Yellow  |
| PA9  (TX1)    | P2:24 (PA9)  | RxD   | Data from STM32 to PC | Green   |
|  -            | P2:33 (GND)  | GND   | Common ground         | Blue    |

## Connect Discovery-F100 to USB-UART 3.3V LVTTL converter
| STM32F100RBT6B | Blue pill   | UART  | Comment (direction)   | Color   |
|:--------------:|:----------- |:----- |:--------------------- |:------- |
| PA10 (RX1)     | P1:8 (PA10) | TxD   | Data from PC to STM32 | Yellow  |
| PA9  (TX1)     | P1:7 (PA9)  | RxD   | Data from STM32 to PC | Green   |
|  -             | P1:1 (GND)  | GND   | Common ground         | Blue    |

## Run minicom
Setup 115200 bod and 8N1 by `minicom -s`
Use /dev/ttyUSB (or ttyACM0):
```
minicom -c on -D /dev/ttyUSB0
```

## Some terminal commands:
```
help        - show help
led 1       - LED on
led 0       - LED off
led blink N - blink led N times 
...
```

## Hot terminal keys:
 - Ctrl+X - show help
 - Ctrl+Q - reboot
 - Ctrl+W - write current config to FLASH (flash write)
 - Ctrl+L - clear terminal window
 - Ctrl+V, Ctrl+S, Ctrl+C, Ctrl+Z, Ctrl+Y, Ctrl+D - unused yet

