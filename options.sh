# Target device
#BOARD="discovery-l100" # Discovery-L100 (STM32L100RCT6)
BOARD="discovery-f100"  # Discovery-F100 (STM32F100RBT6B)
#BOARD="blue_pill"      # "Blue pill" (STM32F103C8T6)

# FLASH/debug software util (supported hardware)
#FLASHER='stlink' # st-flash from stlink-tools Debian package (ST-Link-V2)
FLASHER='openocd' # OpenOCD (ST-Link-V2, J-Link, USB-blaster)
#FLASHER='black-magic' # gdb-arm-none-eabi (Black Magic Probe)

# FLASH/debug hardware interface
INTERFACE='stlink' # SWD mode only
#INTERFACE='jlink' # SWD or Jtag mode
#INTERFACE='usb-blaster' # Jtag mode only

# ST-Link version (select openocd scripts/interface)
STLINK_VER="v1"    # ST-Link v1   (old Discovery dev. board)
#STLINK_VER="v2"   # ST-Link v2   (Discovery dev. board)
#STLINK_VER="v2-1" # ST-Link v2.1 (Nucleo dev. board)

# JTAG or SWD mode by OpenOCD/J-Link
MODE="swd"
#MODE="jtag"

# HEX image
IMAGE="build/${BOARD}.hex"

# OpenOCD target config (chip name)
if [ "$BOARD" == "nucleo-l011k4" ]
then
  TARGET="stm32l0"
elif [ "$BOARD" == "nucleo-l476rg" -o "$BOARD" == "nucleo-l432kc" ]
then
  TARGET="stm32l4x"
elif [ "$BOARD" == "discovery-f051" ]
then
  TARGET="stm32f0"
elif [ "$BOARD" == "stm32_mini" -o "$BOARD" == "stm32_smart"   -o \
       "$BOARD" == "blue_pill"  -o "$BOARD" == "nucleo-f103rb" -o \
       "$BOARD" == "discovery-f100" ]
then
  TARGET="stm32f1x"
elif [ "$BOARD" == "lora-node-151" -o "$BOARD" == "discovery-l100" ]
then
  TARGET="stm32l1"
elif [ "$BOARD" == "stm32_f4ve" ]
then
  TARGET="stm32f4x"
fi
TARGET_CFG="$TARGET.cfg"

# additional option to `st-flash` utility from 'stlink' flasher package
#ST_FLASH_OPT="--flash=128k"

# additional command to `openocd` flasher utility
OPENOCD_OPT="-d2" # verbose level
#FLASH_SIZE="0" # use default FLASH size (~64K)
#FLASH_SIZE="0x10000" # set FLASH size to 64K
#FLASH_SIZE="0x20000" # set FLASH size to 128K
#FLASH_SIZE="0x40000" # set FLASH size to 256K
#OPENOCD_CMD="-c flash bank ${TARGET}_user.flash $TARGET 0x08000000 $FLASH_SIZE 0 0 $TARGET.cpu"

