/*
 * file "disco_test.c"
 */

//-----------------------------------------------------------------------------
//#define DEBUG_PRINT
//-----------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <string.h>
//-----------------------------------------------------------------------------
#include "disco_test.h"
#include "mrl.h"
#include "bsp.h"
#include "print.h"
#include "tim.h"
//#include "syscalls.h"
//-----------------------------------------------------------------------------
// FLASH/EEPROM region for TFS
#include "tfs.h"
#define TFS_NUM 2 // number of FLASH/EEPROM page (1 or 2)
#if defined(STM32L1xx)
#  include "eeprom.h" // use EEPROM for restore/save options
#  define TFS_PAGE_SIZE (4 * EEPROM_PAGE_SIZE)
#  ifndef TFS_ADDRESS
#    define TFS_ADDRESS EEPROM_BASE // EEPROM addres + offset
#  endif
#else
#  include "flash.h" // use FLASH for restore/save options
#  define TFS_PAGE_SIZE 2048
#  ifndef TFS_ADDRESS
#    define TFS_ADDRESS (128*1024 - TFS_PAGE_SIZE * TFS_NUM) // FLASH offset
#  endif
#endif
//-----------------------------------------------------------------------------
#define ABOUT_STRING "\033[35mSTM32 Discovery kit test project\033[0m"
#define VERSION "0.2a"      // soft version
#define COMPL_NUM 100       // max variant of MicroRL complettion
#define MAX_CMD   100       // maximum number of tokens
#define VSNPRINTF_BUF 1024  // buffer used inside cdc_pritf();
//#define CLI_HELP          // CLI help system
//#define EXTRA             // extra functions
//-----------------------------------------------------------------------------
#define KEYCODE_CTRL_D  4 // Ctrl+D key code
#define KEYCODE_CTRL_S 19 // Ctrl+S key code
#define KEYCODE_CTRL_Z 26 // Ctrl+Z key code
#define KEYCODE_CTRL_V 22 // Ctrl+V key code
#define KEYCODE_CTRL_Q 17 // Ctrl+Q key code
#define KEYCODE_CTRL_W 23 // Ctrl+W key code
#define KEYCODE_CTRL_X 24 // Ctrl+X key code
#define KEYCODE_CTRL_Y 25 // Ctrl+Y key code
//-----------------------------------------------------------------------------
#if defined(MRL_USE_COMPLETE) && defined(CLI_HELP)
// array for MicroRL comletion
const char *compl_world[COMPL_NUM + 1];
#endif // MRL_USE_COMPLETE && CLI_HELP
//-----------------------------------------------------------------------------
// global variable(s)
mrl_t mrl; // MicroRL object
tfs_t tfs; // TFS object

volatile uint32_t button_pressed = 0; // increment if button pressed

// all options (saved to / restore from FLASH)
typedef struct {
  int led; // led on/off
  //...

} opt_t;
opt_t opt;
//-----------------------------------------------------------------------------
// command/options description structure
typedef struct cmd_ cmd_t;
struct cmd_ {
  int id;            // ID in tree (>=0)
  int parent_id;     // parent ID for options (or -1 for root)
  void (*fn)(int argc, char* const argv[], const cmd_t*); // callback function
  const char *name;  // command/option name
#ifdef CLI_HELP
  const char *args;  // arguments for help
  const char *help;  // help (description) string 
#endif // CLI_HELP
};
//-----------------------------------------------------------------------------
extern cmd_t const cmd_tree[];
//-----------------------------------------------------------------------------
// get ticks (DWT or SysTicks)
INLINE uint32_t get_ticks()
{
#ifdef BSP_USE_DWT
  return bsp_dwt_ticks();
#else
  return bsp_sys_ticks();
#endif
}
//-----------------------------------------------------------------------------
// send string by USB-CDC or/and USART
static void cdc_print(const char *str)
{
  int len = strlen(str);

#ifdef BSP_USE_USBCDC
  bsp_usbcdc_tx((const uint8_t*) str, len, PRINT_TIMEOUT);
#endif

#ifdef BSP_USART
  bsp_usart_tx((const uint8_t*) str, len, PRINT_TIMEOUT);
#endif

#if !defined(BSP_USE_USBCDC) && !defined(BSP_USART)
  prinf("%s", str);
#endif
}
//-----------------------------------------------------------------------------
// send "\r\n" by USB-CDC or/and USART
static void cdc_endl()
{
  cdc_print("\r\n");
}
//-----------------------------------------------------------------------------
// send line=strinr+"\r\n" by USB-CDC or/and USART
static void cdc_printl(const char *str)
{
  cdc_print(str);
  cdc_endl();
}
//-----------------------------------------------------------------------------
// flush USB-CDC or/and USART TX buffers
INLINE void cdc_flush()
{
#ifdef BSP_USE_USBCDC
  bsp_usbcdc_tx_flush(PRINT_TIMEOUT);
#endif

#ifdef BSP_USART
  bsp_usart_tx_flush(PRINT_TIMEOUT);
#endif

#if !defined(BSP_USE_USBCDC) && !defined(BSP_USART)
  flush(stdout);
#endif
}
//-----------------------------------------------------------------------------
// size of buffer for vsnprintf()
// printf() alternative (send data by USB-CDC or/and USART)
static void cdc_printf(const char *format, ...)
{
  char str[VSNPRINTF_BUF]; // FIXME
  va_list ap;
  va_start(ap, format);
  vsnprintf(str, sizeof(str) - 1, format, ap); // FIXME: add size check
  cdc_print(str);
}
//-----------------------------------------------------------------------------
// set to default all options
static void opt_default(opt_t *o)
{
  o->led = 0; // LED off
  //...
}
//-----------------------------------------------------------------------------
// restore options from FLASH
INLINE void opt_read_from_flash()
{
  uint16_t size, cnt, retv;
  opt_default(&opt);
  retv = tfs_read(&tfs,
                  (void*) &opt, sizeof(opt_t),
                  &size, &cnt);
  if ((retv & ~TFS_ERR_DELETED) == TFS_SUCCESS && size == sizeof(opt_t))
  {
    DBG("read config from FLASH success (size=%i of %i cnt=%i retv=0x%04X)",
         (int) size, (int) sizeof(opt_t), (int) cnt, (unsigned) retv);
  }
  else
  {
    DBG("read config from FLASH fail (retv=0x%04X size=%i of %i cnt=%i)",
        (unsigned) retv, (int) size, (int) sizeof(opt_t),
        (int) cnt);

    DBG("erase FLASH config area");
    tfs_erase(&tfs);
    
    DBG("set to default all options");
    opt_default(&opt);
    
    DBG("write default options to FLASH");
    tfs_write(&tfs, (const void*) &opt, sizeof(opt_t));
  }
}
//-----------------------------------------------------------------------------
// show help based on command/option tree
static void fn_help(int argc, char* const argv[], const cmd_t *cmd)
{ // help
#ifdef CLI_HELP
  if (cmd->id == 0)
  { // root help
    const cmd_t *o;

    cdc_printl(ABOUT_STRING "\r\n"
              "Use TAB key for completion\r\n"
              "Command:");

    for (o = cmd_tree; o->name != NULL; o++)
    {
      uint16_t parents[MAX_CMD];
      int i, parents_cnt = 0;
      const cmd_t *p = o;

      // find all parents
      for (i = 0; i < sizeof(parents) / sizeof(int); i++)
        if (p->parent_id >= 0)
        {
          if (parents_cnt < MAX_CMD)
            parents[parents_cnt++] = p->parent_id;
          p = &cmd_tree[p->parent_id];
        }
        else
          break;

      // print all parents
      cdc_print("  ");
      while (--parents_cnt >= 0)
        cdc_printf("%s ", cmd_tree[parents[parents_cnt]].name);

      // print command and help
      cdc_printf("%s\033[33m%s\033[0m - \033[32m%s\033[0m\r\n",
                 o->name, o->args, o->help);
    } // for
  }
  else
  { // command help
    uint16_t parents[MAX_CMD];
    uint16_t children[MAX_CMD];
    int parents_cnt = 0;
    int children_cnt = 0;
    int i, j;
    const cmd_t *o, *p = cmd;

    // find all parents
    for (i = 0; i < sizeof(parents) / sizeof(int); i++)
      if (p->parent_id >= 0)
      {
        if (parents_cnt < MAX_CMD)
          parents[parents_cnt++] = p->parent_id;
        p = &cmd_tree[p->parent_id];
      }
      else
        break;

    // print all parents
    for (i = --parents_cnt; i >= 0;  i--)
      cdc_printf("%s ", cmd_tree[parents[i]].name);

    // print command help
    cdc_printf("%s:\r\n", cmd->help);

    // find all children
    for (o = cmd_tree; o->name != NULL; o++)
    {
      p = o;
      for (i = 0; i < sizeof(children) / sizeof(int); i++)
      {
        if (p->parent_id == cmd->id)
          if (children_cnt < MAX_CMD)
            children[children_cnt++] = p->id;

        if (p->parent_id >= 0)
          p = &cmd_tree[p->parent_id];
        else
          break;
      }
    }

    // print all children
    for (j = 0; j < children_cnt; j++)
    {
      o = &cmd_tree[children[j]];
      cdc_print("  ");
      for (i = --parents_cnt; i >= 0;  i--)
        cdc_printf("%s ", cmd_tree[parents[i]].name);

      cdc_printf("%s ", cmd->name);
      
      cdc_printf("%s\033[33m%s\033[0m - \033[32m%s\033[0m\r\n",
                 o->name, o->args, o->help);
    }
  }
#else
  if (cmd->id == 0)
    cdc_printl(ABOUT_STRING "\r\n"
               "This build has no internal CLI help");
#endif // CLI_HELP
}
//-----------------------------------------------------------------------------
static void fn_version(int argc, char* const argv[], const cmd_t *cmd)
{ // version
  cdc_printl("version: " VERSION);
#ifdef BUILD_TIME_STAMP
  cdc_printl("build time stamp: " BUILD_TIME_STAMP);
#endif
}
//-----------------------------------------------------------------------------
static void fn_clear(int argc, char* const argv[], const cmd_t *cmd)
{ // clear
  cdc_print("\033[2J"  // ESC seq for clear entire screen
            "\033[H"); // ESC seq for move cursor at left-top corner
}
//-----------------------------------------------------------------------------
static void fn_led(int argc, char* const argv[], const cmd_t *cmd)
{ // led {0|1}
  if (argc == 1)
  {
    opt.led = !!mrl_str2int(argv[0], 0, 10);
    bsp_led2(opt.led);
    cdc_printf("LED %s\r\n", opt.led ? "on" : "off");
  }
}
//-----------------------------------------------------------------------------
static void fn_blink(int argc, char* const argv[], const cmd_t *cmd)
{ // led blink [N]
  int i, n = 1;
  if (argc > 0) n = mrl_str2int(argv[0], 0, 10);
  for (i = 0; i < n; i++)
    bsp_blink_led();
  cdc_printf("blink LED %i times\r\n", n);
}
//-----------------------------------------------------------------------------
static void fn_flash_erase(int argc, char* const argv[], const cmd_t *cmd)
{ // flash erase
  uint16_t retv = tfs_erase(&tfs);
  cdc_printf("FLASH erased (retv=0x%04X)\r\n",
             (unsigned) retv);
}
//-----------------------------------------------------------------------------
static void fn_flash_write(int argc, char* const argv[], const cmd_t *cmd)
{ // flash write
  uint16_t retv;
  bsp_led(1);
  retv = tfs_write(&tfs, (void*) &opt, sizeof(opt_t));
  bsp_led(0);
  cdc_printf("write to FLASH (size=%i retv=0x%04X)\r\n",
             (int) sizeof(opt_t), (unsigned) retv);
}
//-----------------------------------------------------------------------------
static void fn_flash_read(int argc, char* const argv[], const cmd_t *cmd)
{ // flash read
  uint16_t size, cnt, retv;
  retv = tfs_read(&tfs, (void*) &opt, sizeof(opt_t), &size, &cnt);

  if ((retv & ~TFS_ERR_DELETED) == TFS_SUCCESS && size == sizeof(opt_t))
    cdc_printf("read from FLASH success "
               "(size=%i of %i cnt=%i retv=0x%04X)\r\n",
               (int) size, (int) sizeof(opt_t), (int) cnt, (unsigned) retv);
  else
    cdc_printf("read FLASH fail "
               "(retv=0x%04X size=%i of %i cnt=%i)\r\n",
               (unsigned) retv, (int) size, (int) sizeof(opt_t),
               (int) cnt);
}
//-----------------------------------------------------------------------------
static void fn_flash_delete(int argc, char* const argv[], const cmd_t *cmd)
{ // flash delete
  uint16_t retv = tfs_delete(&tfs);
  cdc_printf("delete record from FLASH (retv=0x%04X)\r\n", (unsigned) retv);
}
//-----------------------------------------------------------------------------
static void fn_flash_diff(int argc, char* const argv[], const cmd_t *cmd)
{ // flash diff
  uint16_t retv;
  const uint8_t *data, *ptr = (const uint8_t*) &opt;
  uint16_t len, cnt;
  
  cdc_printl("check FLASH");
  retv = tfs_get(&tfs, (void*) &data, &len, &cnt);
  cdc_printf("tfs_get(): size=%i cnt=%i retv=0x%04X\r\n",
             (int) len, (int) cnt, (unsigned) retv);

  if ((retv & ~TFS_ERR_DELETED) != TFS_SUCCESS)
  {
    cdc_printl("error: can't read record from FLASH; exit");
    return;
  }

  cdc_printf("sizeof(opt_t)=%i\r\n", sizeof(opt_t));
  
  if (len != sizeof(opt_t))
  {
    cdc_printl("error: bad size of record in FLASH; exit");
    return;
  }
  
  for (; len; len--)
    if (*ptr++ != *data++) break;

  if (len == 0)
    cdc_printl("NO DEFFERECE bitween current and saved options");
  else
    cdc_printl("current options and saved are DIFFERENT");
}
//-----------------------------------------------------------------------------
static void fn_flash_dump(int argc, char* const argv[], const cmd_t *cmd)
{ // flash dump [Offset Size]
  uint32_t addr = TFS_ADDRESS;
  int i, size = 16; // 16 bytes by default
  if (argc > 0) addr += (uint32_t) mrl_str2int(argv[0], 0,    10);
  if (argc > 1) size  = (int)      mrl_str2int(argv[1], size, 10);
  cdc_print("FLASH dump (HEX):");
  for (i = 0; i < size; i++)
  {
    uint8_t *ptr = (uint8_t*) (addr + i);
    cdc_printf(" %02X", (unsigned) *ptr);
  }
  cdc_printl("");
}
//-----------------------------------------------------------------------------
_READ_WRITE_RETURN_TYPE _write (int __fd, const void *__buf, size_t __nbyte);
extern const struct __sFILE_fake __sf_fake_stdout;
//-----------------------------------------------------------------------------
static void fn_sys_info(int argc, char* const argv[], const cmd_t *cmd)
{ // sys info
#ifdef EXTRA
  unsigned foo = 0x5A;
  unsigned char *p = (unsigned char *) &foo;

  if (p[sizeof(foo) - 1] == 0x5A)
    cdc_printl("system big indian");
  else if (p[0] == 0x5A)
    cdc_printl("system is little indian");
  else
    cdc_printl("system is unknown indian (?!)");

  cdc_endl();
#ifndef __cplusplus
  cdc_printf("sizeof(void)        = %i\r\n", (int) sizeof(void));
#endif // __cplusplus
  cdc_printf("sizeof(char)        = %i\r\n", (int) sizeof(char));
  cdc_printf("sizeof(short)       = %i\r\n", (int) sizeof(short));
  cdc_printf("sizeof(int)         = %i\r\n", (int) sizeof(int));
  cdc_printf("sizeof(long)        = %i\r\n", (int) sizeof(long));
  cdc_printf("sizeof(long long)   = %i\r\n", (int) sizeof(long long));
  cdc_printf("sizeof(int32_t)     = %i\r\n", (int) sizeof(int32_t));
  cdc_printf("sizeof(int64_t)     = %i\r\n", (int) sizeof(int64_t));
  cdc_printf("sizeof(float)       = %i\r\n", (int) sizeof(float));
  cdc_printf("sizeof(float[2])    = %i\r\n", (int) sizeof(float[2]));
  cdc_printf("sizeof(double)      = %i\r\n", (int) sizeof(double));
  cdc_printf("sizeof(long double) = %i\r\n", (int) sizeof(long double));
  cdc_printf("sizeof(int*)        = %i\r\n", (int) sizeof(int*));
  cdc_printf("sizeof(void*)       = %i\r\n", (int) sizeof(void*));
  cdc_printf("sizeof(time_t)      = %i\r\n", (int) sizeof(time_t));
  cdc_printf("sizeof(size_t)      = %i\r\n", (int) sizeof(size_t));

  cdc_endl();

#ifdef _REENT_GLOBAL_STDIO_STREAMS
  cdc_printl("defined: _REENT_GLOBAL_STDIO_STREAMS");
#endif

#ifdef _REENT_SMALL
  cdc_printl("defined: _REENT_SMALL");
#endif

#ifdef __CUSTOM_FILE_IO__
  cdc_printl("defined: __CUSTOM_FILE_IO__");
#endif
  
#ifdef DEBUG
  cdc_printl("defined: DEBUG");
#endif

#ifdef BSB_USE_USBCDC
  cdc_printl("defined: BSP_USE_USBCDC");
#endif

#ifdef BSP_USBCDC_RX_BUF
  cdc_printf("defined: BSP_USBCDC_RX_BUF = %i\r\n", BSP_USBCDC_RX_BUF);
#endif

#ifdef BSP_USBCDC_TX_BUF
  cdc_printf("defined: BSP_USBCDC_TX_BUF = %i\r\n", BSP_USBCDC_TX_BUF);
#endif

#ifdef BSP_TIM
  cdc_printl("defined: BSP_TIM");
#endif

#ifdef BSP_SPI
  cdc_printl("defined: BSP_SPI");
#endif

#ifdef BSP_USART
  cdc_printl("defined: BSP_USART");
#endif

#ifdef BSP_USART_RX_BUF
  cdc_printf("defined: BSP_USART_RX_BUF = %i\r\n", BSP_USART_RX_BUF);
#endif

#ifdef BSP_USART_TX_BUF
  cdc_printf("defined: BSP_USART_TX_BUF = %i\r\n", BSP_USART_TX_BUF);
#endif

  cdc_endl();
  cdc_printf("SystemCoreClock = %luHz\r\n",
             (uint32_t) SystemCoreClock);
  
  cdc_endl();
  cdc_printf("LL_TIM_IsEnabledCounter() = %i\r\n",
             (int) LL_TIM_IsEnabledCounter(BSP_TIM));
  cdc_printf("LL_SPI_IsEnabled()        = %i\r\n",
             (int) LL_SPI_IsEnabled(BSP_SPI));

  cdc_endl();
  cdc_printf("SysTick->LOAD = %u\r\n",     (unsigned) READ_REG(SysTick->LOAD));
  cdc_printf("SysTick->CTRL = 0x%08X\r\n", (unsigned) READ_REG(SysTick->CTRL));

#if defined(STM32L1xx) || defined(STM32L4xx)
  cdc_endl();
  cdc_printf("RTC->ISR        = 0x%08X\r\n", (unsigned) READ_REG(RTC->ISR));
  cdc_printf("RTC->ISR & WUTF = %i\r\n",     (int) LL_RTC_IsActiveFlag_WUT(RTC));
#endif

  cdc_printf("\r\nsizeof(opt_t) = %i\r\n", sizeof(opt_t));

#endif // EXTRA
}
//-----------------------------------------------------------------------------
static void fn_sys_time(int argc, char* const argv[], const cmd_t *cmd)
{ // sys time
#ifdef EXTRA
  __disable_irq();
  uint32_t t1 = bsp_sys_ticks();
  uint32_t t2 = bsp_tim_ticks();
  uint32_t t3 = bsp_rtc_sec();
  uint32_t t4 = bsp_systicks_dt;
  uint32_t t5 = bsp_systicks_rtc;
  uint32_t t6 = bsp_timticks_rtc;
  uint32_t t7 = READ_REG(SysTick->VAL);
#ifdef BSP_USE_DWT
  uint32_t t8 = bsp_dwt_ticks();
#endif
  __enable_irq();
  cdc_printf("bsp_sys_ticks()  = %lu\r\n", t1);
  cdc_printf("bsp_tim_ticks()  = %lu\r\n", t2);
  cdc_printf("bsp_rtc_sec()    = %lu\r\n", t3);
  cdc_printf("bsp_systicks_dt  = %lu\r\n", t4);
  cdc_printf("bsp_systicks_rtc = %lu\r\n", t5);
  cdc_printf("bsp_timticks_rtc = %lu\r\n", t6);
  cdc_printf("SysTick->VAL     = %lu\r\n", t7);
#ifdef BSP_USE_DWT
  cdc_printf("bsp_dwt_ticks()  = %lu\r\n", t8);
#endif
#endif // EXTRA
}
//-----------------------------------------------------------------------------
static void fn_sys_errors(int argc, char* const argv[], const cmd_t *cmd)
{ // sys errors
#ifdef EXTRA
#ifdef BSP_USART
  uint32_t usart_tx_status;
  uint32_t usart_rx_status;
#endif
#ifdef BSP_USE_USBCDC
  uint32_t usbcdc_tx_status;
  uint32_t usbcdc_rx_status;
#endif

  cdc_flush();

#ifdef BSP_USART
  usart_tx_status  = bsp_usart_tx_status();
  usart_rx_status  = bsp_usart_rx_status();
#endif
#ifdef BSP_USE_USBCDC
  usbcdc_tx_status = bsp_usbcdc_tx_status();
  usbcdc_rx_status = bsp_usbcdc_rx_status();
#endif

#ifdef BSP_USART
#ifdef BSP_USART_RX_BUF
  cdc_printf("bsp_usart_rx_overflow = %lu\r\n", bsp_usart_rx_overflow);
#endif // BSP_USART_RX_BUF
  cdc_printf("bsp_usart_errors      = %lu\r\n", bsp_usart_errors);
  cdc_printf("bsp_usart_tx_status() = %lu\r\n", usart_tx_status);
  cdc_printf("bsp_usart_rx_status() = %lu\r\n", usart_rx_status);
#endif // BSP_USART

#ifdef BSP_USE_USBCDC
  cdc_printf("bsp_usbcdc_rx_overflow = %lu\r\n", bsp_usbcdc_rx_overflow);
  cdc_printf("bsp_usbcdc_tx_status() = %lu\r\n", usbcdc_tx_status);
  cdc_printf("bsp_usbcdc_rx_status() = %lu\r\n", usbcdc_rx_status);
#endif // BSP_USE_USBCDC
#endif // EXTRA
}
//-----------------------------------------------------------------------------
static void fn_sys_reset(int argc, char* const argv[], const cmd_t *cmd)
{ // sys reset
  cdc_endl();
  cdc_flush();
  NVIC_SystemReset();
}
//-----------------------------------------------------------------------------
static void fn_default(int argc, char* const argv[], const cmd_t *cmd)
{ // default
  opt_default(&opt);
}
//-----------------------------------------------------------------------------
static void fn_rtc_time(int argc, char* const argv[], const cmd_t *cmd)
{ // rtc time [0xHHMMSS]
#if defined(STM32L1xx) || defined(STM32L4xx)
  uint32_t time;
  cdc_print("rtc time: ");
  if (argc)
  { // set time
    uint32_t h, m, s;
    time = mrl_str2int(argv[0], 0, 16);
    s =  time        & 0xFF;
    m = (time >> 8)  & 0xFF;
    h = (time >> 16) & 0xFF;

    LL_RTC_DisableWriteProtection(RTC); // disable the write protection
    if (LL_RTC_EnterInitMode(RTC) != ERROR) // set initialization mode
    {
      LL_RTC_TIME_Config(RTC, LL_RTC_FORMAT_BCD, h, m, s);
      LL_RTC_ExitInitMode(RTC); // exit initialization mode
    }
    LL_RTC_EnableWriteProtection(RTC); // enable the write protection
    cdc_print("set ");
  }
    
  time = LL_RTC_TIME_Get(RTC);
  cdc_printf("0x%06X (0xHHMMSS)\r\n", (unsigned) time);
    
#elif defined(STM32F1xx)
  cdc_print("rtc time: ");
  if (argc)
  { // set time
    bsp_rtcsec = mrl_str2int(argv[0], 0, 0);
    cdc_print("set ");
  }
  cdc_printf("%u sec\r\n", (unsigned) bsp_rtcsec);
#endif
}
//-----------------------------------------------------------------------------
static void fn_rtc_date(int argc, char* const argv[], const cmd_t *cmd)
{ // rtc date [0xWWDDMMYY]
#if defined(STM32L1xx) || defined(STM32L4xx)
  uint32_t date;

  cdc_print("rtc date: ");

  if (argc)
  { // set date
    uint32_t y, m, d, w;
    date = mrl_str2int(argv[0], 0, 16);

    y =  date        & 0xFF;
    m = (date >> 8)  & 0xFF;
    d = (date >> 16) & 0xFF;
    w = (date >> 24) & 0xFF;
    
    LL_RTC_DisableWriteProtection(RTC); // disable the write protection
    if (LL_RTC_EnterInitMode(RTC) != ERROR) // set initialization mode
    {
      LL_RTC_DATE_Config(RTC, w, d, m, y);
      //LL_RTC_DisableInitMode(RTC); // exit initialization mode
      LL_RTC_ExitInitMode(RTC); // exit initialization mode
    }
    LL_RTC_EnableWriteProtection(RTC); // enable the write protection

    cdc_print("set ");
  }
  date = LL_RTC_DATE_Get(RTC);
  
  cdc_printf("0x%08X (0xWWDDMMYY)\r\n", (unsigned) date);
#elif defined(STM32F1xx)
  cdc_printl("STM32F1xx has no RTC date low layer API");
#endif
}
//-----------------------------------------------------------------------------
static void fn_rtc_subsecond(int argc, char* const argv[], const cmd_t *cmd)
{ // rtc subsecond
#if defined(STM32L1xx) || defined(STM32L4xx)
  uint32_t ss = LL_RTC_TIME_GetSubSecond(RTC);
#elif defined(STM32F1xx)
  uint32_t ss = LL_RTC_TIME_Get(RTC);
#else
  uint32_t ss = 0;
#endif
  cdc_printf("rtc subsecond: %u\r\n", (unsigned) ss);
}
//-----------------------------------------------------------------------------
static void fn_sleep(int argc, char* const argv[], const cmd_t *cmd)
{ // sleep [M CNT]
  int n = 0, m = 1;
  if (argc > 0) n = mrl_str2int(argv[0], 0, 0);
  if (argc > 1) m = mrl_str2int(argv[1], 0, 0);

  while (m--)
  {
    if (n == 0)
    {
      cdc_printl("go to SLEEP mode");
      cdc_flush();
      bsp_sleep_mode();
    }
    else if (n == 1)
    {
      cdc_printl("go to Low-Power SLEEP mode");
      cdc_flush();
      bsp_lpsleep_mode();
    }
    else if (n == 2)
    {
      cdc_printl("go to STOP mode");
      cdc_flush();
      bsp_stop_mode();
    }
    else if (n == 3)
    {
      cdc_printl("go to STANDBY mode");
      cdc_flush();
      bsp_standby_mode();
    }
  }
}
//-----------------------------------------------------------------------------
#ifdef CLI_HELP
#  define _F(id, parent_id, func, name, args, help) \
   { id, parent_id, func, name, args, help }, // CLI help ON
#else
#  define _F(id, parent_id, func, name, args, help) \
   { id, parent_id, func, name }, // CLI help OFF
#endif
//-----------------------------------------------------------------------------
// all commands and options tree
cmd_t const cmd_tree[] = {
  // ID Par  Callback           Name         Args               Help
  _F( 0, -1, fn_help,           "help",      "",                "print this help (Ctrl+X)")
  _F( 1, -1, fn_version,        "version",   "",                "print version")
  _F( 2, -1, fn_clear,          "clear",     "",                "clear screen (Ctrl+L)")
  _F( 3, -1, fn_led,            "led",       " {0|1}",          "1=on/0=off LED")
  _F( 4,  3, fn_blink,          "blink",     " [N]",            "blink LED N times")
  _F( 5, -1, fn_help,           "flash",     "",                "FLASH commands")
  _F( 6,  5, fn_flash_erase,    "erase",     "",                "erase options area of FLASH")
  _F( 7,  5, fn_flash_write,    "write",     "",                "write all options to FLASH (Ctrl+W)")
  _F( 8,  5, fn_flash_read,     "read",      "",                "read all options from FLASH")
  _F( 9,  5, fn_flash_delete,   "delete",    "",                "delete last options record from FLASH")
  _F(10,  5, fn_flash_diff,     "diff",      "",                "find differece between current options and saved in FLASH")
  _F(11,  5, fn_flash_dump,     "dump",      " [Offset Size]",  "hex dump region in FLASH")
  _F(12, -1, fn_help,           "sys",       "",                "system information")
  _F(13, 12, fn_sys_info,       "info",      "",                "print system information")
  _F(14, 12, fn_sys_time,       "time",      "",                "print system time and ticks")
  _F(15, 12, fn_sys_errors,     "errors",    "",                "print all system errors")
  _F(16, 12, fn_sys_reset,      "reset",     "",                "full system reset MCU")
  _F(17, -1, fn_default,        "default",   "",                "set to default all options")
  _F(18, -1, fn_help,           "rtc",       "",                "get/set RTC")
#if defined(STM32L1xx) || defined(STM32L4xx)
  _F(19, 18, fn_rtc_time,       "time",      " [0xHHMMSS]",     "get/set RTC time in BCD formar")
#elif defined(STM32F1xx)
  _F(19, 18, fn_rtc_time,       "time",      " [sec]",          "get/set RTC time in seconds")
#endif
  _F(20, 18, fn_rtc_date,       "date",      " [0xWWDDMMYY]",   "get/set RTC date in BCD format")
  _F(21, 18, fn_rtc_subsecond,  "subsecond", "",                "get RTC subsecond [0-255]")
  _F(22, -1, fn_sleep,          "sleep",     " [M [CNT]]",      "go to STM32x SLEEP mode (0-sleep, 1-lpsleep, 2-stop, 3-standby)")
  _F(23, -1, NULL,              NULL,        NULL,              NULL)
};
//----------------------------------------------------------------------------
#undef _F
//----------------------------------------------------------------------------
// execute callback for microrl library
static void execute_cb(int argc, char * const argv[])
{
  int i, parent_id = -1, arg_shift;
  const cmd_t *found = (cmd_t*) NULL;

  for (i = 0; i < argc; i++)
  {
    const cmd_t *cmd = cmd_tree;
    while (cmd->name != NULL)
    {
      if (cmd->parent_id == parent_id && strcmp(cmd->name, argv[i]) == 0)
      { // command/option found
        found     = cmd;
        parent_id = cmd->id;
        arg_shift = i + 1;
        break;
      }
      cmd++;
    } // while
  } // for

  if (found != (cmd_t*) NULL) // command found
    found->fn(argc - arg_shift, argv + arg_shift, found);
  else
    printf("command %s not found\r\n", argv[0]);
}
//-----------------------------------------------------------------------------
#if defined(MRL_USE_COMPLETE) && defined(CLI_HELP)
// completion callback for microrl library
static const char** complete_cb(int argc, char * const argv[])
{
  int i, parent_id = -1, count = 0;
  
  for (i = 0; i < argc ; i++)
  {
    const cmd_t *cmd = cmd_tree;
    while (cmd->name != NULL)
    {
      if (cmd->parent_id == parent_id &&
          strstr(cmd->name, argv[i]) == cmd->name &&
          (parent_id != -1 || i == 0))
      { // substring found => add it to completion set

        if (i == argc - 1 && count < COMPL_NUM)
          compl_world[count++] = cmd->name;

        if (strcmp(cmd->name, argv[i]) == 0)
        { // command/option full found
          parent_id = cmd->id;
          break;
        }
      }
      cmd++;
    } // while
  } // for

  compl_world[count] = NULL;
  return compl_world;
}
#endif // MRL_USE_COMPLETE && CLI_HELP
//-----------------------------------------------------------------------------
// button callback
void button_callback()
{
  button_pressed++;
  bsp_blink_led();  
  cdc_printl("\r\nButton pressed");
  mrl_refresh(&mrl);
}
//-----------------------------------------------------------------------------
// timer callback
void timer_callback()
{
  // do nothing
}
//-----------------------------------------------------------------------------
// Ctrl+C callback
void sigint_cb()
{
  cdc_printl("\r\nCtr+C pressed");
  //...
  mrl_refresh(&mrl);
}
//-----------------------------------------------------------------------------
// main function of project
void disco_test()
{
  int i;

  // init BSP
  bsp_init();

#if 0
  // blink forever
  while (1)
  {
    bsp_led(0);
    bsp_led2(1);
    bsp_sleep(500);
    bsp_led(1);
    bsp_led2(0);
    bsp_sleep(500);
  }
#endif
  
#if 1
  // blink LED 3 times #1
  for (i = 0; i < 3; i++)
  {
    bsp_led(1);
    bsp_led2(1);
    bsp_sleep(100);
    bsp_led(0);
    bsp_led2(0);
    bsp_sleep(100);
  }
#endif
  
#ifdef BSP_USE_USBCDC
  // pause 1 sec
  //bsp_delay(1000);
  bsp_sleep(1000);

  // blink LED 3 times #2
  for (i = 0; i < 3; i++)
    bsp_blink_led();

  // whait USB...
  bsp_sleep(1000);
#endif

  // LED on - start init
  bsp_led(1);

  // print debug/system info
#ifdef DEBUG
  cdc_endl();
  fn_version(0, NULL, NULL);
  cdc_endl();
  fn_sys_info(0, NULL, NULL);
  cdc_endl();
  fn_sys_time(0, NULL, NULL);
  cdc_endl();
#endif

  // init command parser
  mrl_init(&mrl, cdc_print);
  mrl_set_execute_cb(&mrl, execute_cb);
#if defined(MRL_USE_COMPLETE) && defined(CLI_HELP)
  mrl_set_complete_cb(&mrl, complete_cb); // set callback for completion
#endif // MRL_USE_COMPLETE && CLI_HELP
#ifdef MRL_USE_CTRL_C
  mrl_set_sigint_cb(&mrl, sigint_cb);
#endif // MRL_USE_CTRL_C
  DBG("mrl_init()");
  
  // init FLASH region
  tfs_init(&tfs, TFS_NUM, TFS_ADDRESS, TFS_PAGE_SIZE);

  // restore config from FLASH
  opt_read_from_flash();

  // print ready message
#ifdef EXTRA
  cdc_printl(ABOUT_STRING);
#endif // EXTRA

  // show prompt
  mrl_prompt(&mrl);
  cdc_flush();

  // LED off - init finish
  bsp_led(0);

  // restore LED state
  bsp_led2(opt.led);

  // main loop forever
  while (1)
  {
    char key = 0;
    
    // main loop
    //...
    
#ifdef BSP_USE_USBCDC
    // check data from USB-CDC buffer
    while (bsp_usbcdc_rx((uint8_t*) &key, 1, 0) == 1)
    {
      //cdc_printf("key=0x%02X\r\n", key);
      if ((key = mrl_insert_char(&mrl, key)) != 0)
        break;
    }
    
    // flush USB-CDC output buffer
    bsp_usbcdc_tx_flush(0);
#endif

#ifdef BSP_USART
    // check data from USART buffer
    while (bsp_usart_rx((uint8_t*) &key, 1, 0) == 1)
      if ((key = mrl_insert_char(&mrl, key)) != 0)
        break;
#endif

    // check special keys
    if (key == KEYCODE_CTRL_D) // Ctrl+D pressed
    {
      cdc_printl("\r\nCtrl+D pressed");
      //...
      mrl_refresh(&mrl);
    }
    else if (key == KEYCODE_CTRL_S) // Ctrl+S pressed
    {
      cdc_printl("\r\nCtrl+S pressed");
      //...
      mrl_refresh(&mrl);
    }
    else if (key == KEYCODE_CTRL_Z) // Ctrl+Z pressed
    {
      cdc_printl("\r\nCtrl+Z pressed");
      //...
      mrl_refresh(&mrl);
    }
    else if (key == KEYCODE_CTRL_V) // Ctrl+V pressed
    {
      cdc_printl("\r\nCtrl+V pressed");
      //...
      mrl_refresh(&mrl);
    }
    else if (key == KEYCODE_CTRL_Q) // Ctrl+Q pressed
    {
      cdc_printl("\r\nCtrl+Q pressed");
      cdc_printl("reboot...");
      bsp_led(1);
      cdc_flush();
      NVIC_SystemReset();
    }
    else if (key == KEYCODE_CTRL_W) // Ctrl+W pressed
    {
      cdc_printl("\r\nCtrl+W pressed");
      fn_flash_write(0, NULL, NULL);
      mrl_refresh(&mrl);
    }
    else if (key == KEYCODE_CTRL_X) // Ctrl+X pressed
    {
      cdc_printl("\r\nCtrl+X pressed");
      fn_help(0, NULL, &cmd_tree[0]);
      mrl_refresh(&mrl);
    }
    else if (key == KEYCODE_CTRL_Y) // Ctrl+Y pressed
    {
      cdc_printl("\r\nCtrl+Y pressed");
      //...
      mrl_refresh(&mrl);
    }
  } // while (1)
}
//-----------------------------------------------------------------------------

/*** end of "disco_test.c" ***/

