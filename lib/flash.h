/*
 * STM32 FLASH wrappers
 * file "flash.h"
 */

#ifndef FLASH_H
#define FLASH_H
//-----------------------------------------------------------------------------
#include <string.h>
#include "bsp.h"
//-----------------------------------------------------------------------------
#if defined(STM32L4xx)
#  ifndef FLASH_KEY1
#    define FLASH_KEY1 0x45670123U
#  endif
#  ifndef FLASH_KEY2
#    define FLASH_KEY2 0xCDEF89ABU
#  endif
#  ifndef FLASH_BANK 
#    define FLASH_BANK 0
#  endif
#  ifndef FLASH_PAGE_SIZE
#    define FLASH_PAGE_SIZE 2048 // STM32L4/L4+ FLASH page size
#  endif
#elif defined(STM32F1xx)
#  ifndef FLASH_PAGE_SIZE
#    define FLASH_PAGE_SIZE 1024 // STM32F1 FLASH page size
#  endif
#endif
//-----------------------------------------------------------------------------
#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus
//-----------------------------------------------------------------------------
// unlock FLASH
INLINE void flash_unlock()
{
  if (READ_BIT(FLASH->CR, FLASH_CR_LOCK))
  {
    WRITE_REG(FLASH->KEYR, FLASH_KEY1); // 0x45670123
    WRITE_REG(FLASH->KEYR, FLASH_KEY2); // 0xCDEF89AB
  }
}
//-----------------------------------------------------------------------------
// lock FLASH
INLINE void flash_lock()
{
  SET_BIT(FLASH->CR, FLASH_CR_LOCK);
} 
//-----------------------------------------------------------------------------
// check FLASH locked
INLINE uint8_t flash_locked()
{
  return !!READ_BIT(FLASH->CR, FLASH_CR_LOCK);
} 
//-----------------------------------------------------------------------------
// read FLASH
INLINE void flash_read(uint8_t *data, uint32_t address, uint32_t count)
{
  memcpy((void*) data, (const void*) address, (size_t) count);
}
//-----------------------------------------------------------------------------
// erase FLASH page
// (return: 0-sucsess, -1-BSY timeout, -2-EOP timeout, -3-locked)
int8_t flash_erase(uint32_t address, uint32_t page_num,
                   uint32_t timeout);
//-----------------------------------------------------------------------------
// write data to FLASH
// (return: 0-sucsess, -1-BSY timeout, -2-EOP timeout, -3-locked)
int8_t flash_write(uint32_t address, const uint8_t *data, uint32_t count,
                   uint32_t timeout);
//-----------------------------------------------------------------------------
#ifdef __cplusplus
}
#endif // __cplusplus
//-----------------------------------------------------------------------------
#endif // FLASH_H

/*** end of "flash.h" file ***/

