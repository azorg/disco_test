/*
 * Board Support Package functions (BSP)
 * file "bsp.c"
 */

//-----------------------------------------------------------------------------
#include "bsp.h"
#include "print.h"
//-----------------------------------------------------------------------------
//void SystemClock_Config(void); // CubeMX system clock config
//-----------------------------------------------------------------------------
#ifdef BSP_USE_USBCDC
#  include "usbd_cdc_if.h" // CDC_Transmit_FS()
#  ifndef BSP_USBCDC_TX_BUF
#    define BSP_USBCDC_TX_BUF 1024
#  endif
#  ifndef BSP_USBCDC_RX_BUF
#    define BSP_USBCDC_RX_BUF 1024
#  endif
#endif
//-----------------------------------------------------------------------------
// global variables
#if BSP_USE_LED
int  bsp_blink_led_on    = 50;  // LED on time [ms]
int  bsp_blink_led_off   = 100; // LED off time [ms]
char bsp_blink_led_state = 0;   // LED state (0-off, 1-on)
int  bsp_blink_led_cnt   = 0;   // LED time counter
int  bsp_blink_led_times = 0;   // LED blink counter
#endif

volatile uint32_t bsp_systicks = 0; // SysTick counter
volatile uint32_t bsp_timticks = 0; // timer counter
volatile uint32_t bsp_rtcsec   = 0; // RTC second counter

volatile uint32_t bsp_systicks_dt  = 1000; // delta SyTick counter on RTC
volatile uint32_t bsp_systicks_rtc = 0; // SysTick counter on RTC wakeup
volatile uint32_t bsp_timticks_rtc = 0; // timer counter on RTC wakeup

static volatile uint8_t bsp_deepsleep = 0; // deep sleep flag {0|1}

#ifdef BSP_USART
#  ifdef BSP_USART_RX_BUF
volatile uint8_t  bsp_usart_rx_buf[BSP_USART_RX_BUF];
volatile uint32_t bsp_usart_rx_wr_index = 0;
volatile uint32_t bsp_usart_rx_rd_index = 0;
volatile uint32_t bsp_usart_rx_count    = 0;
volatile uint32_t bsp_usart_rx_overflow = 0;
#  endif // BSP_USART_RX_BUF

#  ifdef BSP_USART_TX_BUF
volatile uint8_t  bsp_usart_tx_buf[BSP_USART_TX_BUF];
volatile uint32_t bsp_usart_tx_wr_index = 0;
volatile uint32_t bsp_usart_tx_rd_index = 0;
volatile uint32_t bsp_usart_tx_count    = 0;
volatile uint8_t  bsp_usart_tx_now      = 0;
#  endif // BSP_USART_TX_BUF

volatile uint32_t bsp_usart_errors = 0;
#endif // BSP_USART

#ifdef BSP_USE_USBCDC
volatile uint8_t  bsp_usbcdc_rx_buf[BSP_USBCDC_RX_BUF];
volatile uint32_t bsp_usbcdc_rx_wr_index = 0;
volatile uint32_t bsp_usbcdc_rx_rd_index = 0;
volatile uint32_t bsp_usbcdc_rx_count    = 0;
volatile uint32_t bsp_usbcdc_rx_overflow = 0;

volatile uint8_t  bsp_usbcdc_tx_buf[BSP_USBCDC_TX_BUF];
volatile uint32_t bsp_usbcdc_tx_wr_index = 0;
volatile uint32_t bsp_usbcdc_tx_rd_index = 0;
volatile uint32_t bsp_usbcdc_tx_count    = 0;
#endif // BSP_USE_USBCDC
//-----------------------------------------------------------------------------
// restore clocks after STOP mode (deep sleep)
// (look SystemClock_Config() and patch it)
INLINE void bsp_restore_clock()
{
#if defined(STM32L100xC) // Discovery-L100
  //LL_PWR_SetRegulVoltageScaling(LL_PWR_REGU_VOLTAGE_SCALE1);
  LL_RCC_HSI_Enable();

  // wait till HSI is ready
  while (LL_RCC_HSI_IsReady() != 1) { }
  LL_RCC_HSI_SetCalibTrimming(16);
  LL_RCC_LSI_Enable();

  // wait till LSI is ready
  while (LL_RCC_LSI_IsReady() != 1) { }
  LL_PWR_EnableBkUpAccess();
  LL_RCC_ForceBackupDomainReset();
  LL_RCC_ReleaseBackupDomainReset();
  LL_RCC_SetRTCClockSource(LL_RCC_RTC_CLKSOURCE_LSI);
  LL_RCC_EnableRTC();
  LL_RCC_PLL_ConfigDomain_SYS(LL_RCC_PLLSOURCE_HSI, LL_RCC_PLL_MUL_6, LL_RCC_PLL_DIV_3);
  LL_RCC_PLL_Enable();

  // wait till PLL is ready
  while (LL_RCC_PLL_IsReady() != 1) { }
  LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);
  LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_1);
  LL_RCC_SetAPB2Prescaler(LL_RCC_APB2_DIV_1);
  LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_PLL);

  // wait till System clock is ready
  while (LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_STATUS_PLL) { }
  LL_Init1msTick(32000000);
  LL_SYSTICK_SetClkSource(LL_SYSTICK_CLKSOURCE_HCLK);
  LL_SetSystemCoreClock(32000000);

#elif defined(STM32L151xC) // LoRa-Node-151
  LL_RCC_HSE_Enable();

  // wait till HSE is ready
  while (LL_RCC_HSE_IsReady() != 1) { }

  //LL_PWR_EnableBkUpAccess();
  //LL_RCC_ForceBackupDomainReset();
  //LL_RCC_ReleaseBackupDomainReset();
  
  //LL_RCC_LSE_Enable();
  
  // wait till LSE is ready
  //while (LL_RCC_LSE_IsReady() != 1) { }
  //LL_RCC_SetRTCClockSource(LL_RCC_RTC_CLKSOURCE_LSE);
  //LL_RCC_EnableRTC(); // set RTCEN
  
  //LL_RCC_PLL_ConfigDomain_SYS(LL_RCC_PLLSOURCE_HSE, LL_RCC_PLL_MUL_12, LL_RCC_PLL_DIV_3);
  LL_RCC_PLL_Enable();

  // wait till PLL is ready
  while (LL_RCC_PLL_IsReady() != 1) { }
  LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);
  LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_1);
  LL_RCC_SetAPB2Prescaler(LL_RCC_APB2_DIV_1);
  LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_PLL);

  // wait till System clock is ready
  while (LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_STATUS_PLL) { }
  LL_SetSystemCoreClock(32000000);

#elif defined(STM32L4xx) // Nucleo-L4xxx
  LL_RCC_HSI_Enable();

  // wait till HSI is ready
  while (LL_RCC_HSI_IsReady() != 1) { }
  LL_RCC_HSI_SetCalibTrimming(16);
  
  //LL_PWR_EnableBkUpAccess();
  //LL_RCC_ForceBackupDomainReset();
  //LL_RCC_ReleaseBackupDomainReset();
  //LL_RCC_LSE_SetDriveCapability(LL_RCC_LSEDRIVE_LOW);
  
  //LL_RCC_LSE_Enable();

  // wait till LSE is ready
  //while (LL_RCC_LSE_IsReady() != 1) { }
  //LL_RCC_SetRTCClockSource(LL_RCC_RTC_CLKSOURCE_LSE);
  //LL_RCC_EnableRTC();

  //LL_RCC_PLL_ConfigDomain_SYS(LL_RCC_PLLSOURCE_HSI, LL_RCC_PLLM_DIV_1, 20, LL_RCC_PLLR_DIV_4);
  //LL_RCC_PLL_EnableDomain_SYS();
  LL_RCC_PLL_Enable();

  // wait till PLL is ready
  while (LL_RCC_PLL_IsReady() != 1) { }
  LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_PLL);

  // wait till System clock is ready
  while (LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_STATUS_PLL) { }
  LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);
  LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_1);
  LL_RCC_SetAPB2Prescaler(LL_RCC_APB2_DIV_1);

  //LL_Init1msTick(80000000);
   
  LL_SYSTICK_SetClkSource(LL_SYSTICK_CLKSOURCE_HCLK);
  LL_SetSystemCoreClock(80000000);
  //LL_RCC_SetUSARTClockSource(LL_RCC_USART2_CLKSOURCE_PCLK1);
  //LL_RCC_SetI2CClockSource(LL_RCC_I2C1_CLKSOURCE_PCLK1);

#elif defined(STM32F1xx) // Blue pill / STM32 Smart / Nucleo-F103 / Discovery-F100
  LL_RCC_HSE_Enable();

  // wait till HSE is ready
  while (LL_RCC_HSE_IsReady() != 1) { }

  //LL_PWR_EnableBkUpAccess();
  //LL_RCC_ForceBackupDomainReset();
  //LL_RCC_ReleaseBackupDomainReset();
  
  //LL_RCC_LSE_Enable();
  
  // wait till LSE is ready
  //while (LL_RCC_LSE_IsReady() != 1) { }
  //LL_RCC_SetRTCClockSource(LL_RCC_RTC_CLKSOURCE_LSE);
  //LL_RCC_EnableRTC(); // set RTCEN

  //LL_RCC_PLL_ConfigDomain_SYS(LL_RCC_PLLSOURCE_HSE_DIV_1, LL_RCC_PLL_MUL_9);
  LL_RCC_PLL_Enable();

  // wait till PLL is ready
  while (LL_RCC_PLL_IsReady() != 1) { }
  LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);
  LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_2);
  LL_RCC_SetAPB2Prescaler(LL_RCC_APB2_DIV_1);
  LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_PLL);
  
  // wait till System clock is ready
  while (LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_STATUS_PLL) { }
  LL_SetSystemCoreClock(72000000);
#ifdef BSP_USE_USBCDC
  LL_RCC_SetUSBClockSource(LL_RCC_USB_CLKSOURCE_PLL_DIV_1_5);
#endif
#endif
}
//-----------------------------------------------------------------------------
// init RTC
static void bsp_init_rtc()
{
#if defined(STM32L100xC) // Discovery-L100
  // enable Wakeup RTC time (fix CubeMX bad code)
  LL_RTC_DisableWriteProtection(RTC); // disable write protection (set CNF)
  LL_RTC_WAKEUP_Disable(RTC); // disable Wakeup timer
  while (LL_RTC_IsActiveFlag_WUTW(RTC) == 0) { }
  LL_RTC_WAKEUP_SetClock(RTC, LL_RTC_WAKEUPCLOCK_DIV_8); // 37000/8=4625Hz
  LL_RTC_WAKEUP_SetAutoReload(RTC, 4503); // 4625-1 => 4625/4625~1Hz tick
                                          // FIXME: magic
  LL_RTC_WAKEUP_Enable(RTC); // enable wakeup timer
  LL_RTC_EnableIT_WUT(RTC); // enable wakeup timer interrupt
  LL_RTC_EnableWriteProtection(RTC); // enable write protection (clear CNF)
  
  // init EXTI line 20 for wakeup
  LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_20); // EXTI->PR
  LL_EXTI_EnableRisingTrig_0_31(LL_EXTI_LINE_20); // EXTI->RTSR
  LL_EXTI_EnableIT_0_31(LL_EXTI_LINE_20); // EXTI->IMR
  
  //NVIC_EnableIRQ(RTC_WKUP_IRQn);

#elif defined(STM32L151xC) // LoRa-Node-151
  // enable Wakeup RTC time (fix CubeMX bad code)
  LL_RTC_DisableWriteProtection(RTC); // disable write protection (set CNF)
  LL_RTC_WAKEUP_Disable(RTC); // disable Wakeup timer
  while (LL_RTC_IsActiveFlag_WUTW(RTC) == 0) { }
  LL_RTC_WAKEUP_SetClock(RTC, LL_RTC_WAKEUPCLOCK_DIV_16); // 32768/16=2048Hz
  LL_RTC_WAKEUP_SetAutoReload(RTC, 2047); // 2048-1 => 2048/2048=1Hz tick
  LL_RTC_WAKEUP_Enable(RTC); // enable wakeup timer
  LL_RTC_EnableIT_WUT(RTC); // enable wakeup timer interrupt
  LL_RTC_EnableWriteProtection(RTC); // enable write protection (clear CNF)
  
  // init EXTI line 20 for wakeup
  LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_20); // EXTI->PR
  LL_EXTI_EnableRisingTrig_0_31(LL_EXTI_LINE_20); // EXTI->RTSR
  LL_EXTI_EnableIT_0_31(LL_EXTI_LINE_20); // EXTI->IMR

#elif defined(STM32L4xx) // Nucleo-L4xx
  LL_RTC_DisableWriteProtection(RTC); // disable write protection (set CNF)
  LL_RTC_SetHourFormat(RTC, LL_RTC_HOURFORMAT_24HOUR); 
  LL_RTC_SetAsynchPrescaler(RTC, 127); // 32768/(127+1) = 256Hz
  LL_RTC_SetSynchPrescaler(RTC, 255);  // 256/(255+1) = 1Hz
  LL_RTC_DisableShadowRegBypass(RTC);  //!!!
  LL_RTC_WAKEUP_Disable(RTC); // disable Wakeup timer
  while (LL_RTC_IsActiveFlag_WUTW(RTC) == 0) { }
  LL_RTC_WAKEUP_SetClock(RTC, LL_RTC_WAKEUPCLOCK_DIV_16); // 32768/16=2048Hz
  LL_RTC_WAKEUP_SetAutoReload(RTC, 2047); // 2048-1 => 2048/2048=1Hz tick
  LL_RTC_WAKEUP_Enable(RTC); // enable wakeup timer
  LL_RTC_EnableIT_WUT(RTC); // enable wakeup timer interrupt
  LL_RTC_EnableWriteProtection(RTC); // enable write protection (clear CNF)
  
  // init EXTI line 20 for wakeup
  LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_20); // EXTI->PR
  LL_EXTI_EnableRisingTrig_0_31(LL_EXTI_LINE_20); // EXTI->RTSR
  LL_EXTI_EnableIT_0_31(LL_EXTI_LINE_20); // EXTI->IMR
  
  //NVIC_EnableIRQ(RTC_WKUP_IRQn);

#elif defined(STM32F1xx) // Bleu pill / STM32 Smart / Nucleo-F103 / Discovery-F100
  // enable Alarm RTC (fix CubeMX bad code)
  while (LL_RTC_IsActiveFlag_RTOF(RTC) == 0) { }
  LL_RTC_DisableWriteProtection(RTC); // disable write protection (set CNF)
  LL_RTC_SetAsynchPrescaler(RTC, 255); // 256-1 => 32768/256=128Hz
  LL_RTC_TIME_Set(RTC, 0); // set time counter [0..0xFFFFF]
  LL_RTC_ALARM_Set(RTC, 127); // set alarm counter // T=1sec
  LL_RTC_ClearFlag_ALR(RTC); // clear ALRF
  LL_RTC_DisableIT_ALR(RTC); // disable alarm interrupt (clear ALRIE) => use EXTI-17
  LL_RTC_EnableWriteProtection(RTC); // enable write protection (clear CNF)

#ifndef STM32F100xB // Discovery-F100
  // init EXTI line 17 for wakeup
  LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_17); // EXTI->PR
  //LL_EXTI_EnableRisingTrig_0_31(LL_EXTI_LINE_17); // EXTI->RTSR
  LL_EXTI_EnableFallingTrig_0_31(LL_EXTI_LINE_17); // EXTI->FTSR
  LL_EXTI_EnableIT_0_31(LL_EXTI_LINE_17); // EXTI->IMR
#endif
#endif
}
//-----------------------------------------------------------------------------
// init BSP
void bsp_init()
{
  // set SysTick frquency to 1kHz and enable interrupt
#if 1
  SysTick_Config((uint32_t) SystemCoreClock / 1000);
#else
  LL_Init1msTick(SystemCoreClock);
  LL_SYSTICK_EnableIT(); // set SysTick_CTRL_TICKINT_Msk
#endif
  
#ifdef BSP_USE_LED
  bsp_led(0);
#endif
      
#ifdef BSP_USART
#ifdef BSP_USART_RX_BUF
  LL_USART_EnableIT_RXNE(BSP_USART); // enable interrupt from RXNE
#endif
#ifdef BSP_USART_TX_BUF
  LL_USART_DisableIT_TXE(BSP_USART); // disable interrupt from TXE
  LL_USART_DisableIT_TC(BSP_USART);  // disable interrupt from TC
#endif
#endif

  // SPI enable
  LL_SPI_Enable(BSP_SPI);

  // USART enable
  //LL_USART_Enable(BSP_USART);
  
#if defined(STM32L4xx)
  LL_SPI_SetRxFIFOThreshold(BSP_SPI, LL_SPI_RX_FIFO_TH_QUARTER);
#endif

#ifdef USART_CR1_FIFOEN // IS_UART_FIFO_INSTANCE(BSP_USART)
  LL_USART_DisableFIFO(BSP_USART);
#endif
  
  // reset SLEEPONEXIT bit
  LL_LPM_DisableSleepOnExit();

  // reset SLEEPDEEP bit of Cortex System Control Register
  LL_LPM_EnableSleep();

  // wait one SysTick interrupt
  bsp_sleep(1);

  // reset all counters
#ifdef BSP_USE_DWT
  BSP_DWT_ENABLE();
  BSP_DWT_CYCCNT = 0;
#endif
  bsp_systicks = 0;
  bsp_timticks = 0;
  bsp_rtcsec   = 0;

#if 1 && defined(BSP_TIM)
  // start timer
  LL_TIM_EnableIT_UPDATE(BSP_TIM);
  LL_TIM_EnableCounter(BSP_TIM);
#endif
  
  // enable RTC
  bsp_init_rtc();
}
//-----------------------------------------------------------------------------
// go to STANDBY mode
void bsp_standby_mode()
{
  // disable interrupt
  __disable_irq();

  // set SLEEPDEEP bit of Cortex System Control Register
  LL_LPM_EnableDeepSleep(); // set SLEEPDEEP

#if defined(STM32L1xx)
  // set Power Down mode when CPU enters deepsleep
  LL_PWR_SetPowerMode(LL_PWR_MODE_STANDBY); // set PDDS

  // set voltage Regulator mode during low power modes
  LL_PWR_SetRegulModeLP(LL_PWR_REGU_LPMODES_LOW_POWER); // set LPSDSR

  // enable fast wakeup by ignoring VREFINT startup time when exiting from low-power mode
  LL_PWR_EnableFastWakeUp(); // set FWU

#elif defined(STM32L4xx)
  LL_PWR_SetPowerMode(LL_PWR_MODE_STANDBY); // LPMS
  
  LL_PWR_EnableInternWU(); // EIWF

#elif defined(STM32F1xx)
  // set Power Down mode when CPU enters deepsleep
  LL_PWR_SetPowerMode(LL_PWR_MODE_STANDBY); // clear LPDS, set PDDS
  
  // set voltage Regulator mode during deep sleep mode
  //LL_PWR_SetRegulModeDS(LL_PWR_REGU_DSMODE_LOW_POWER); // set LPDS 
  //LL_PWR_SetRegulModeDS(LL_PWR_REGU_DSMODE_MAIN); // clear LPDS
#endif

  // clear Standby Flag
  LL_PWR_ClearFlag_SB(); // set CSBF

  // data synchronization barrier
  __DSB();
  
  // go to STANDBY mode (power down)
  __WFI();
}
//-----------------------------------------------------------------------------
// go to STOP mode (deep sleep)
void bsp_stop_mode()
{
  // disable interrupt
  __disable_irq();

  // set SLEEPDEEP bit of Cortex System Control Register
  LL_LPM_EnableDeepSleep(); // set SLEEPDEEP
  
#if defined(STM32L1xx)
  // clear Power Down mode when CPU enters deepsleep
  LL_PWR_SetPowerMode(LL_PWR_MODE_STOP); // clear PDDS

  // set voltage Regulator mode during low power modes
  LL_PWR_SetRegulModeLP(LL_PWR_REGU_LPMODES_LOW_POWER); // set LPSDSR

  // enable fast wakeup by ignoring VREFINT startup time when exiting from low-power mode
  LL_PWR_EnableFastWakeUp(); // set FWU
  
#elif defined(STM32L4xx)
  LL_PWR_SetPowerMode(LL_PWR_MODE_STOP2); // LPMS
  //LL_PWR_SetPowerMode(LL_PWR_MODE_STOP1); // LPMS
  //LL_PWR_SetPowerMode(LL_PWR_MODE_STOP0); // LPMS
 
  //LL_PWR_EnableInternWU(); // EIWF
  
  //LL_RTC_ClearFlag_WUT(RTC); // clear WUTF
  //LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_0);

#elif defined(STM32F1xx)
  // set Power Down mode when CPU enters deepsleep
  LL_PWR_SetPowerMode(LL_PWR_MODE_STOP_LPREGU); // set LPDS, clear PDDS
  //LL_PWR_SetPowerMode(LL_PWR_MODE_STOP_MAINREGU); // clear LPDS, clear PDSS

  // set voltage Regulator mode during deep sleep mode
  //LL_PWR_SetRegulModeDS(LL_PWR_REGU_DSMODE_LOW_POWER); // set LPDS 
  //LL_PWR_SetRegulModeDS(LL_PWR_REGU_DSMODE_MAIN); // clear LPDS

  // disable APB1 peripherals clock
  //LL_APB1_GRP1_DisableClock(LL_APB1_GRP1_PERIPH_PWR);
#endif
 
  // clear Wake-up flags
  LL_PWR_ClearFlag_WU(); // set CWUF
  
  // set flag; reset it in IRQ handler later
  bsp_deepsleep = 1;

  // data synchronization barrier
  __DSB();

  // go to STOP mode and wait RTC interrupt
  __WFI();
  
#if defined(STM32L1xx)
  // set voltage Regulator mode during low power modes
  LL_PWR_SetRegulModeLP(LL_PWR_REGU_LPMODES_MAIN); // clear LPSDSR

#elif defined(STM32L4xx)
  //...

#elif defined(STM32F1xx)
  // enable APB1 peripherals clock
  //LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_PWR);
  
  // set voltage Regulator mode during deep sleep mode
  LL_PWR_SetRegulModeDS(LL_PWR_REGU_DSMODE_MAIN); // clear LPDS
#endif

  // restore clocks
  bsp_led(1);
  bsp_restore_clock();
  bsp_led(0);

  // clear SLEEPDEEP bit of Cortex System Control Register
  LL_LPM_EnableSleep(); // clear SLEEPDEEP

#if defined(STM32L100xC) // Discovery-L100
  // restore RTC
  bsp_init_rtc();

  // restore SysTick
  SysTick_Config((uint32_t) SystemCoreClock / 1000);
#endif
  
  // enable interrupt
  __enable_irq();
}
//-----------------------------------------------------------------------------
// go to FAKE STOP mode (wait RTC interrupt for debug)
void bsp_fake_stop_mode()
{
 uint32_t sec = bsp_rtcsec;
 while (sec == bsp_rtcsec)
   bsp_sleep_mode();
}
//-----------------------------------------------------------------------------
// go to RUN mode
void bsp_run_mode()
{
#if defined(STM32L1xx)
  // set voltage Regulator mode during low power modes
  LL_PWR_SetRegulModeLP(LL_PWR_REGU_LPMODES_MAIN); // clear LPSDSR
#endif
  
  // rclear SLEEPDEEP bit of Cortex System Control Register
  LL_LPM_EnableSleep(); // clear SLEEPDEEP
}
//-----------------------------------------------------------------------------
// go to SLEEP mode (wait for any interrupt)
void bsp_sleep_mode()
{
  // clear SLEEPDEEP bit of Cortex System Control Register
  LL_LPM_EnableSleep(); // clear SLEEPDEEP
  
  // wait for interrupt
  __WFI();
}
//-----------------------------------------------------------------------------
// go to Low-Power SLEEP mode (wait for any interrupt)
void bsp_lpsleep_mode()
{
  // clear SLEEPDEEP bit of Cortex System Control Register
  LL_LPM_EnableSleep(); // clear SLEEPDEEP

#if defined(STM32L1xx)
  // set the main internal Regulator output voltage (1.5V - range 2)
  //LL_PWR_SetRegulVoltageScaling(LL_PWR_REGU_VOLTAGE_SCALE2); // VOS0, VOS1

  // set voltage Regulator mode during low power modes
  LL_PWR_SetRegulModeLP(LL_PWR_REGU_LPMODES_LOW_POWER); // set LPSDSR
  
  // switch the Regulator from main mode to low-power mode
  LL_PWR_EnableLowPowerRunMode(); // set LPRUN
  
  // enable ultra low-power mode by enabling VREFINT switch off in low-power modes
  LL_PWR_EnableUltraLowPower(); // set ULP

  // wait for interrupt
  __WFI();

  // disable ultra low-power mode by disabling VREFINT switch off in low-power modes
  LL_PWR_DisableUltraLowPower(); // clear ULP

  // switch the Regulator from low-power mode to main mode
  LL_PWR_DisableLowPowerRunMode(); // clear LPRUN
  
  // set voltage Regulator mode during low power modes
  LL_PWR_SetRegulModeLP(LL_PWR_REGU_LPMODES_MAIN); // clear LPSDSR

#elif defined(STM32L4xx)
  LL_PWR_EnableLowPowerRunMode(); // LPR
  LL_PWR_EnterLowPowerRunMode(); // LPR
  
  // wait for interrupt
  __WFI();
  
  LL_PWR_ExitLowPowerRunMode(); // LRR
  LL_PWR_DisableLowPowerRunMode(); // LPR

#else // MCU hasn't Low-Power sleep mode
  // wait for interrupt
  __WFI();
#endif
}
//-----------------------------------------------------------------------------
// go to SLEEP mode for some milliseconds (wait system timer interrupt)
void bsp_sleep(uint32_t ms)
{
  uint32_t tickstart = bsp_systicks;
  while ((bsp_systicks - tickstart) < ms)
    __WFI(); // wait for interrupt
}
//-----------------------------------------------------------------------------
// loop-delay based on System Timer (do not sleep)
void bsp_delay(uint32_t ms)
{
#if 1
  uint32_t tickstart = bsp_systicks;
  while ((bsp_systicks - tickstart) < ms) {;}
#else
  LL_mDelay((uint32_t) ms);
#endif
}
//-----------------------------------------------------------------------------
// loop-delay based on DWT_SYSCNT
#ifdef BSP_USE_DWT
void bsp_delay_us(uint32_t us)
{
  uint32_t cycles = (SystemCoreClock / 1000000L) * us;
  uint32_t start = BSP_DWT_CYCCNT;
  while (BSP_DWT_CYCCNT - start < cycles) {;}
}
#endif
//-----------------------------------------------------------------------------
// on/off onboard LED
void bsp_led(int on)
{
#if BSP_USE_LED
#if defined(LD3_GPIO_Port) && defined(LD3_Pin) // Nucleo-Lx / Discovery-Fx
  if (on) LL_GPIO_SetOutputPin  (LD3_GPIO_Port, LD3_Pin);
  else    LL_GPIO_ResetOutputPin(LD3_GPIO_Port, LD3_Pin);
#elif defined(LD2_GPIO_Port) && defined(LD2_Pin) // Nucleo-F103RB
  if (on) LL_GPIO_SetOutputPin  (LD2_GPIO_Port, LD2_Pin);
  else    LL_GPIO_ResetOutputPin(LD2_GPIO_Port, LD2_Pin);
#elif defined(LED_GPIO_Port) && defined(LED_Pin) // STM32_mini / LoRa-Node-151
  if (on) LL_GPIO_SetOutputPin  (LED_GPIO_Port, LED_Pin);
  else    LL_GPIO_ResetOutputPin(LED_GPIO_Port, LED_Pin);
#elif defined(NLED_GPIO_Port) && defined(NLED_Pin) // blue_pill / STM32_Smart
  if (on) LL_GPIO_ResetOutputPin(NLED_GPIO_Port, NLED_Pin);
  else    LL_GPIO_SetOutputPin  (NLED_GPIO_Port, NLED_Pin);
#endif

#if defined(LORA_NODE_151) && defined(PB3_GPIO_Port) && defined(PB3_Pin)
  if (on) LL_GPIO_ResetOutputPin(PB3_GPIO_Port, PB3_Pin);
  else    LL_GPIO_SetOutputPin  (PB3_GPIO_Port, PB3_Pin);
#endif

#endif // BSP_USE_LED
}
//-----------------------------------------------------------------------------
void bsp_led2(int on)
{
#if BSP_USE_LED
#  if defined(LD4_GPIO_Port) && defined(LD4_Pin) // Discovery
  if (on) LL_GPIO_SetOutputPin  (LD4_GPIO_Port, LD4_Pin);
  else    LL_GPIO_ResetOutputPin(LD4_GPIO_Port, LD4_Pin);
#  endif
#endif
}
//-----------------------------------------------------------------------------
// blink onboard LED
void bsp_blink_led()
{
#if BSP_USE_LED
  bsp_blink_led_times++;
#endif
}
//-----------------------------------------------------------------------------
#ifdef BSP_SPI
// SPI exchange wrapper function
// return:
//    0 - on error (timeout)
//    1 - on success
uint8_t bsp_spi_exchange(
  uint8_t       *rx_buf,  // RX buffer
  const uint8_t *tx_buf,  // TX buffer
  uint32_t      size,     // number of bytes
  uint32_t      timeout)  // timeout [ms]
{
  for (; size != 0; size--)
  {
    // wait TXE ready
    uint32_t start = bsp_systicks;
    while (!LL_SPI_IsActiveFlag_TXE(BSP_SPI))
    {
      if (bsp_systicks - start >= timeout)
        return 0; // timeout
    }

    // transmit byte
    LL_SPI_TransmitData8(BSP_SPI, *tx_buf++);

    // wait RXNE ready
    start = bsp_systicks;
    while (!LL_SPI_IsActiveFlag_RXNE(BSP_SPI))
      if (bsp_systicks - start >= timeout)
      {
	fprintf(stderr, "RXNE timeout\r\n");
        return 0; // timeout
      }

    // receive byte
    *rx_buf++ = LL_SPI_ReceiveData8(BSP_SPI);
  }

  return 1; // success
}
#endif // BSP_SPI
//-----------------------------------------------------------------------------
#ifdef BSP_USART
// TX data to USART
// (return number of sent bytes)
uint32_t bsp_usart_tx(
  const uint8_t *data, // TX data
  uint32_t size,       // number of bytes
  uint32_t timeout)    // timeout [ms]
{
#ifdef BSP_USART_TX_BUF
  uint32_t start = bsp_systicks;
  uint32_t count = 0;
  bsp_usart_tx_now = !!size;
  while (count < size)
  {
    while (bsp_usart_tx_count == BSP_USART_TX_BUF)
    { // output buffer FULL
      if (bsp_systicks - start >= timeout)
        return count; // timeout
      __WFI();
    }

    LL_USART_DisableIT_TXE(BSP_USART); // disable TXE interrupt
    if (bsp_usart_tx_count || !LL_USART_IsActiveFlag_TXE(BSP_USART))
    { // append to TX buffer
      bsp_usart_tx_buf[bsp_usart_tx_wr_index] = *data++;
      bsp_usart_tx_count++;
      LL_USART_EnableIT_TXE(BSP_USART); // enable TXE interrupt
      if (++bsp_usart_tx_wr_index == BSP_USART_TX_BUF)
        bsp_usart_tx_wr_index = 0;
      count++;
    }
    else
    {
      LL_USART_TransmitData8(BSP_USART, *data++);
      count++;
    }
  } // while (count < size)
  return size;

#else
  uint32_t i;
  for (i = 0; i < size; i++)
  {
#if 0 && defined(STM32L4xx)
    // wait BUSY/TXE FIXME: ??!
    uint32_t start = bsp_systicks;
    while (LL_USART_IsActiveFlag_BUSY(BSP_USART) ||
	   !LL_USART_IsActiveFlag_TXE(BSP_USART))
      if (bsp_systicks - start >= timeout)
        return 0; // timeout
#else
    // wait TXE
    uint32_t start = bsp_systicks;
    while (!LL_USART_IsActiveFlag_TXE(BSP_USART))
      if (bsp_systicks - start >= timeout)
        return 0; // timeout
#endif

    // transmit byte
    LL_USART_TransmitData8(BSP_USART, *data++);

#if 0 && defined(STM32L4xx)
    // wait TC FIXME: ??!
    start = bsp_systicks;
    while (!LL_USART_IsActiveFlag_TC(BSP_USART))    
      if (bsp_systicks - start >= timeout)
        return 0; // timeout
#endif
  }
  return size;
#endif
}
//-----------------------------------------------------------------------------
// check TX USART buffer status
// (return number of bytes in TX buffer)
uint32_t bsp_usart_tx_status()
{
#ifdef BSP_USART_TX_BUF
  return bsp_usart_tx_count + !LL_USART_IsActiveFlag_TXE(BSP_USART);
#else
  return !LL_USART_IsActiveFlag_TXE(BSP_USART);
#endif
}
//-----------------------------------------------------------------------------
// flush TX USART buffer
// (return number bytes in TX buffer at the end of timeout)
uint32_t bsp_usart_tx_flush(uint32_t timeout) // timeout [ms]
{
  uint32_t start = bsp_systicks;

#ifdef BSP_USART_TX_BUF
  while (bsp_usart_tx_now)
  { // TX not complete
    if (bsp_systicks - start >= timeout)
      break; // timeout
    __WFI();
  }
  return bsp_usart_tx_count;

#else
  while (!LL_USART_IsActiveFlag_TXE(BSP_USART) ||
         !LL_USART_IsActiveFlag_TC(BSP_USART))
  { // TX register not empty or TX not complete
    if (bsp_systicks - start >= timeout)
      return 1; // timeout
  }
  return 0;
#endif
}
//-----------------------------------------------------------------------------
// RX data from USART
// (return number of received bytes 0...size)
uint32_t bsp_usart_rx(
  uint8_t *data,     // RX data buffer
  uint32_t size,     // size of RX buffer
  uint32_t timeout)  // timeout [ms]
{
#ifdef BSP_USART_RX_BUF
  uint32_t start = bsp_systicks;
  uint32_t count = 0;
  while (count < size)
  {
    while (bsp_usart_rx_count == 0)
    { // input buffer EMPTY
      if (bsp_systicks - start >= timeout)
        return count; // timeout
      __WFI();
    }
    
    *data++ = bsp_usart_rx_buf[bsp_usart_rx_rd_index];
    if (++bsp_usart_rx_rd_index == BSP_USART_RX_BUF)
      bsp_usart_rx_rd_index = 0;
    
    LL_USART_DisableIT_RXNE(BSP_USART); // disable interrupt
    bsp_usart_rx_count--;
    LL_USART_EnableIT_RXNE(BSP_USART); // enable interrupt
    count++;
  } // while (count < size)
  return size;

#else
  uint32_t i;
  for (i = 0; i < size; i++)
  { // wait RXNE
    uint32_t start = bsp_systicks;
    while (!LL_USART_IsActiveFlag_RXNE(BSP_USART))
    {
      if (bsp_systicks - start >= timeout)
        return i; // timeout
      __WFI();
    }

    // receive byte
    *data++ = LL_USART_ReceiveData8(BSP_USART);
  }
  return i;
#endif
}
//-----------------------------------------------------------------------------
// check RX USART buffer status
// (return number of bytes in RX buffer)
uint32_t bsp_usart_rx_status()
{
#ifdef BSP_USART_RX_BUF
  return bsp_usart_rx_count;
#else
  return !!LL_USART_IsActiveFlag_RXNE(BSP_USART);
#endif
}
//-----------------------------------------------------------------------------
// USART callback (called from "stm32f1xx_it.c")
void bsp_usart_callback()
{
#if defined(BSP_USART_RX_BUF) || defined(BSP_USART_TX_BUF)

#if defined(STM32L4xx)
  uint32_t isr = READ_REG(BSP_USART->ISR); // status register (STM32L4xx)
#else
  uint32_t sr  = READ_REG(BSP_USART->SR);  // status register
#endif
  uint32_t cr1 = READ_REG(BSP_USART->CR1); // control register #1

#ifdef BSP_USART_RX_BUF
#if defined(STM32L4xx)
  if ((isr & LL_USART_ISR_RXNE) && (cr1 & LL_USART_CR1_RXNEIE))
#else
  if ((sr & LL_USART_SR_RXNE) && (cr1 & LL_USART_CR1_RXNEIE))
#endif
  { // RX register Not Empty
    if (bsp_usart_rx_count < BSP_USART_RX_BUF)
    {
      bsp_usart_rx_buf[bsp_usart_rx_wr_index] = LL_USART_ReceiveData8(BSP_USART);
      if (++bsp_usart_rx_wr_index == BSP_USART_RX_BUF)
        bsp_usart_rx_wr_index = 0;
      bsp_usart_rx_count++;
    }
    else // RX buffer overflow :(
    {
      __IO uint32_t tmpreg = LL_USART_ReceiveData8(BSP_USART); 
      (void) tmpreg; // lost data
      bsp_usart_rx_overflow++;
    }
  }
#endif

#ifdef BSP_USART_TX_BUF
#if defined(STM32L4xx)
  if ((isr & LL_USART_ISR_TXE) && (cr1 & LL_USART_CR1_TXEIE))
#else
  if ((sr & LL_USART_SR_TXE) && (cr1 & LL_USART_CR1_TXEIE))
#endif
  { // TXE - TX register empty
    if (bsp_usart_tx_count)
    { // output buffer not empty (paranoic check)
      LL_USART_TransmitData8(BSP_USART,
                             bsp_usart_tx_buf[bsp_usart_tx_rd_index]);
      if (++bsp_usart_tx_rd_index == BSP_USART_TX_BUF)
        bsp_usart_tx_rd_index = 0;
      if (--bsp_usart_tx_count == 0)
      { // last byte transmitted from buffer
        LL_USART_DisableIT_TXE(BSP_USART); // disable TXE interrupt
        LL_USART_EnableIT_TC(BSP_USART);   // enable TC interrupt
      }
    }
  }

#if defined(STM32L4xx)
  if ((isr & LL_USART_ISR_TC) && (cr1 & LL_USART_CR1_TCIE))
#else
  if ((sr & LL_USART_SR_TC) && (cr1 & LL_USART_CR1_TCIE))
#endif
  { // TC - transmission complete
    LL_USART_DisableIT_TC(BSP_USART); // disable TC interrupt
    bsp_usart_tx_now = 0;
  }
#endif

#if defined(STM32L4xx)
  if (isr & (LL_USART_ISR_ORE | // over run error
	     LL_USART_ISR_FE  | // framing error
             LL_USART_ISR_PE  | // patity error
             LL_USART_ISR_NE))  // noise error
#else
  if (sr & (LL_USART_SR_ORE | // over run error
            LL_USART_SR_FE  | // framing error 
            LL_USART_SR_PE  | // patity error
            LL_USART_SR_NE))  // noise error
#endif
  { // error
    __IO uint32_t tmpreg = LL_USART_ReceiveData8(BSP_USART);
    (void) tmpreg;
    bsp_usart_errors++;
  }
#endif // BSP_USART_RX_BUF || BSP_USART_TX_BUF
}
#endif // BSP_USART
//-----------------------------------------------------------------------------
#ifdef BSP_USE_USBCDC
// TX data to USB-CDC (write to buffer)
// (return number of sent bytes)
uint32_t bsp_usbcdc_tx(
  const uint8_t *data, // TX data
  uint32_t size,       // number of bytes
  uint32_t timeout)    // timeout [ms]
{
  uint32_t start = bsp_systicks;
  uint32_t part, max_part, count = 0;
  while ((part = size - count) != 0)
  {
    while ((max_part = BSP_USBCDC_TX_BUF - bsp_usbcdc_tx_count) == 0 ||
           CDC_Transmit_FS((uint8_t*) NULL, (uint16_t) 0) == USBD_BUSY)
    { // output buffer is FULL or USB-CDC driver is busy
      if (bsp_systicks - start >= timeout)
        return count; // timeout
      __WFI();
      bsp_usbcdc_tx_flush(0);
    }

    __disable_irq(); // disable interrupt FIXME: need disable only USB interrupt
    max_part = BSP_USBCDC_TX_BUF - bsp_usbcdc_tx_count; // free bytes
    if (part > max_part) part = max_part;
    max_part = BSP_USBCDC_TX_BUF - bsp_usbcdc_tx_wr_index;
    if (part > max_part) part = max_part;
    if (part)
    {
      memcpy((void*) bsp_usbcdc_tx_buf + bsp_usbcdc_tx_wr_index,
             (const void*) data, part);
      bsp_usbcdc_tx_wr_index += part;
      if (bsp_usbcdc_tx_wr_index == BSP_USBCDC_TX_BUF)
        bsp_usbcdc_tx_wr_index = 0;
      bsp_usbcdc_tx_count += part;
      data  += part;
      count += part;
    }

    part = size - count;
    max_part = BSP_USBCDC_TX_BUF - bsp_usbcdc_tx_count; // free bytes
    if (part > max_part) part = max_part;
    if (part)
    {
      memcpy((void*) bsp_usbcdc_tx_buf,
             (const void*) data, part);
      bsp_usbcdc_tx_wr_index = part;
      bsp_usbcdc_tx_count += part;
      data  += part;
      count += part;
    }
    __enable_irq(); // enable interrupt FIXME: need disable only USB interrupt
  } // while ((part = size - count) != 0)
  return count;
}
//-----------------------------------------------------------------------------
// check TX USB-CDC buffer status
// (return number of bytes in TX buffer)
uint32_t bsp_usbcdc_tx_status()
{
  return bsp_usbcdc_tx_count;
}
//-----------------------------------------------------------------------------
// flush TX data from output USB-CDC buffer 
// (return USBD_OK, USBD_BUSY or error code)
uint8_t bsp_usbcdc_tx_flush(uint32_t timeout) // timeout ms
{
  uint8_t retv;
  uint32_t start = bsp_systicks, part, max_part;
  while ((part = bsp_usbcdc_tx_count) != 0)
  {
    while (CDC_Transmit_FS((uint8_t*) NULL, (uint16_t) 0) == USBD_BUSY)
    { // USB-CDC driver is busy
      if (bsp_systicks - start >= timeout)
        return USBD_BUSY; // timeout
      __WFI();
    }
    
    max_part = BSP_USBCDC_TX_BUF - bsp_usbcdc_tx_rd_index;
    if (part > max_part) part = max_part;
    if (part)
    {
      retv = CDC_Transmit_FS((uint8_t*) bsp_usbcdc_tx_buf + bsp_usbcdc_tx_rd_index,
                             (uint16_t) part);
      if (retv == USBD_OK)        
      {
        bsp_usbcdc_tx_rd_index += part;
        if (bsp_usbcdc_tx_rd_index == BSP_USBCDC_TX_BUF)
          bsp_usbcdc_tx_rd_index = 0;
        bsp_usbcdc_tx_count -= part;
      }
      else
        return retv;
    }

    while (CDC_Transmit_FS((uint8_t*) NULL, (uint16_t) 0) == USBD_BUSY)
    { // USB-CDC driver is busy
      if (bsp_systicks - start >= timeout)
        return USBD_BUSY; // timeout
      __WFI();
    }

    part = bsp_usbcdc_tx_count;
    if (part)
    {
      retv = CDC_Transmit_FS((uint8_t*) bsp_usbcdc_tx_buf,
                             (uint16_t) part);
      if (retv == USBD_OK)
      {
        bsp_usbcdc_tx_rd_index = part;
        bsp_usbcdc_tx_count   -= part;
      }
      else
        return retv;
    }      
  } // while ((part = bsp_usbcdc_tx_count) != 0)
  return USBD_OK;
}
//-----------------------------------------------------------------------------
// RX data from USB-CDC
// (return number of received bytes 0...size)
uint32_t bsp_usbcdc_rx(
  uint8_t *data,     // RX data buffer
  uint32_t size,     // size of RX buffer
  uint32_t timeout)  // timeout [ms]
{
  uint32_t start = bsp_systicks;
  uint32_t part, max_part, count = 0;
  while ((part = size - count) != 0)
  {
    while (bsp_usbcdc_rx_count == 0)
    { // input buffer EMPTY
      if (bsp_systicks - start >= timeout)
        return count; // timeout
      __WFI();
    }

    __disable_irq(); // disable interrupt FIXME: need disable only USB interrupt
    max_part = BSP_USBCDC_RX_BUF - bsp_usbcdc_rx_rd_index;
    if (part > max_part) part = max_part;
    if (part > bsp_usbcdc_rx_count) part = bsp_usbcdc_rx_count;
    if (part)
    {
      memcpy((void*) data,
             (const void*) bsp_usbcdc_rx_buf + bsp_usbcdc_rx_rd_index,
             part);
      bsp_usbcdc_rx_rd_index += part;
      if (bsp_usbcdc_rx_rd_index == BSP_USBCDC_RX_BUF)
        bsp_usbcdc_rx_rd_index = 0;
      bsp_usbcdc_rx_count -= part;
      data  += part;
      count += part;
    }
    
    part = size - count;
    if (part > bsp_usbcdc_rx_count) part = bsp_usbcdc_rx_count;
    if (part)
    {
      memcpy((void*) data, (const void*) bsp_usbcdc_rx_buf, part);
      bsp_usbcdc_rx_rd_index = part;
      bsp_usbcdc_rx_count   -= part;
      data  += part;
      count += part;
    }
    __enable_irq(); // enable interrupt FIXME: need disable only USB interrupt
  } // while ((part = size - count) != 0)
  return count;
}
//-----------------------------------------------------------------------------
// check RX USB-CDC buffer status
// (return number of bytes in RX buffer)
uint32_t bsp_usbcdc_rx_status()
{
  return bsp_usbcdc_rx_count;
}
//-----------------------------------------------------------------------------
// USB-CDC callback (called from "usbd_cdc_if.c")
// (return received/saved bytes)
uint32_t bsp_usbcdc_rx_callback(uint8_t *buf, uint32_t size)
{
  uint32_t max_part, count, part;

  part = size;
  max_part = BSP_USBCDC_RX_BUF - bsp_usbcdc_rx_wr_index;
  if (part > max_part) part = max_part;
  max_part = BSP_USBCDC_RX_BUF - bsp_usbcdc_rx_count; // free bytes
  if (part > max_part) part = max_part;
  if (part)
  {
    memcpy((void*) bsp_usbcdc_rx_buf + bsp_usbcdc_rx_wr_index,
           (const void*) buf, part);
    bsp_usbcdc_rx_wr_index += part;
    if (bsp_usbcdc_rx_wr_index == BSP_USBCDC_RX_BUF)
      bsp_usbcdc_rx_wr_index = 0;
    bsp_usbcdc_rx_count += part;
    buf += part;
  }

  count = part;
  part = size - count;
  max_part = BSP_USBCDC_RX_BUF - bsp_usbcdc_rx_count; // free bytes
  if (part > max_part) part = max_part;
  if (part)
  {
    memcpy((void*) bsp_usbcdc_rx_buf,
           (const void*) buf + count,
           part);
    bsp_usbcdc_rx_wr_index = part;
    bsp_usbcdc_rx_count += part;
    count += part;
  }
    
  if (count != size)
    bsp_usbcdc_rx_overflow++;

  return count;
}
#endif // BSP_USE_USBCDC
//-----------------------------------------------------------------------------
// SysTick timer callback 1kHz (called from "stm32xxxx_it.c")
void bsp_systick_callback()
{
  if (!LL_SYSTICK_IsActiveCounterFlag()) return;

#if 1 && defined(BSP_USE_LED)
  // blink LED via system timer
  if (bsp_blink_led_times)
  {
    if (bsp_blink_led_cnt == 0)
    {
      bsp_blink_led_cnt = bsp_blink_led_on;
      bsp_led(bsp_blink_led_state = 1);
    }
    else // bsp_blink_led_cnt != 0
    {
      if (--bsp_blink_led_cnt == 0)
      {
        if (bsp_blink_led_state)
        {
          bsp_blink_led_cnt = bsp_blink_led_off;
          bsp_led(bsp_blink_led_state = 0);
        }
        else
        {
          bsp_blink_led_times--;
        }
      }
    }
  }
#endif // BSP_USE_LED
  
  bsp_systicks++; // increment SysTick counter
}
//-----------------------------------------------------------------------------
#ifdef BSP_TIM
// timer callback (called from "stm32xxxx_it.c")
void bsp_timer_callback()
{
  timer_callback(); // user callback
  
  bsp_timticks++; // increment timer counter
}
#endif // BSP_TIM
//-----------------------------------------------------------------------------
// RTC callback 1Hz (called from "stm32xxxx_it.c")
void bsp_rtc_callback()
{
  bsp_rtcsec++; // RTC second counter

  if (bsp_deepsleep)
  { // update bsp_systicks and bsp_timticks from deep sleep
    bsp_deepsleep    = 0;
    bsp_systicks_dt  = 1000;
    bsp_systicks     = bsp_systicks_rtc + 1000; // 1kHz
    bsp_timticks     = bsp_timticks_rtc + 100;  // 100Hz
    bsp_systicks_rtc = bsp_systicks;
    bsp_timticks_rtc = bsp_timticks;
  }
  else
  {
    bsp_systicks_dt  = bsp_systicks - bsp_systicks_rtc;
    bsp_systicks_rtc = bsp_systicks;
    bsp_timticks_rtc = bsp_timticks;
  }
}
//-----------------------------------------------------------------------------

/*** end of "bsp.c" file ***/

