/*
 * file "print.c"
 */

//-----------------------------------------------------------------------------
#include <string.h>
#include "print.h"
#include "bsp.h"
//-----------------------------------------------------------------------------
// print string
int print_str(const char *msg)
{
  int len = strlen(msg);
#if defined(PRINT_SWO)
  int i;
  for (i = 0; i < len; i++)
    ITM_SendChar((uint32_t) msg[i]);
  return len;
#elif defined(PRINT_USART)
  return bsp_usart_tx((const uint8_t*) msg, len, PRINT_TIMEOUT);
#elif defined(PRINT_USB)
  int rv = bsp_usbcdc_tx((const uint8_t*) msg, len, PRINT_TIMEOUT);
  return rv;
#else
  return 0;
#endif
}
//-----------------------------------------------------------------------------
// print char
int print_chr(char c)
{
#if defined(PRINT_SWO)
  ITM_SendChar((uint32_t) c);
  return 1;
#elif defined(PRINT_USART)
  return bsp_usart_tx((const uint8_t*) &c, 1, PRINT_TIMEOUT);
#elif defined(PRINT_USB)
  int rv = bsp_usbcdc_tx((const uint8_t*) &c, 1, PRINT_TIMEOUT);
  return rv;
#else
  return 0;
#endif
}
//-----------------------------------------------------------------------------
// print integer value
int print_int(int i)
{
#if defined(PRINT_SWO) || defined(PRINT_USART) || defined(PRINT_USB)
  char minus = 0;
  char buf[12];
  char *ptr = buf + sizeof(buf);

  if (i < 0)
  {
    i = -i;
    minus = 1;
  }

  *--ptr = '\0';
  do {
    *--ptr = '0' + (i % 10);
    i /= 10;
  } while (i != 0);

  if (minus)
    *--ptr = '-';

  return print_str(ptr);
#else
  return 0;
#endif
}
//-----------------------------------------------------------------------------
// hex print unsigned integer value
int print_hex(unsigned int i, char digits)
{
#if defined(PRINT_SWO) || defined(PRINT_USART) || defined(PRINT_USB)
  char buf[8];
  char *ptr = buf + sizeof(buf);

  *--ptr = '\0';
  while (digits-- > 0)
  {
    int c = i & 0xf;
    if (c < 10)
      c += '0';
    else
      c += 'A' - 10;
    *--ptr = c;
    i >>= 4;
  }

  return print_str(ptr);
#else
  return 0;
#endif
}
//-----------------------------------------------------------------------------

/*** end of "print.c" file ***/

