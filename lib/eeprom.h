/*
 * STM32 EEPROM wrappers
 * file "eeprom.h"
 */

#ifndef EEPROM_H
#define EEPROM_H
//-----------------------------------------------------------------------------
#include <string.h>
#include "bsp.h"
//-----------------------------------------------------------------------------
#ifndef EEPROM_BASE
#define EEPROM_BASE FLASH_EEPROM_BASE // 0x08080000
#endif
#ifndef EEPROM_SIZE
#define EEPROM_SIZE 4096
#endif
#ifndef EEPROM_PAGE_SIZE
#define EEPROM_PAGE_SIZE 256 // fake page size
#endif
//-----------------------------------------------------------------------------
#if defined(STM32L100xC)
#  ifndef FLASH_PEKEY1
#    define FLASH_PEKEY1 (0x89ABCDEFU)
#  endif
#  ifndef FLASH_PEKEY2
#    define FLASH_PEKEY2 (0x02030405U)
#  endif
#  ifndef FLASH_FLAG_BSY
#    define FLASH_FLAG_BSY FLASH_SR_BSY
#  endif
#endif
//-----------------------------------------------------------------------------
#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus
//-----------------------------------------------------------------------------
// unlock EEPROM
INLINE void eeprom_unlock()
{
  if (READ_BIT(FLASH->PECR, FLASH_PECR_PELOCK))
  { // unlocking the Data memory and FLASH_PECR register access
    FLASH->PEKEYR = FLASH_PEKEY1;
    FLASH->PEKEYR = FLASH_PEKEY2;
  }
}
//-----------------------------------------------------------------------------
// lock EEPROM
INLINE void eeprom_lock()
{
  // set the PELOCK Bit to lock the data memory and FLASH_PECR register access
  SET_BIT(FLASH->PECR, FLASH_PECR_PELOCK);
} 
//-----------------------------------------------------------------------------
// check EEPROM locked
INLINE uint8_t eeprom_locked()
{
  return !!READ_BIT(FLASH->PECR, FLASH_PECR_PELOCK);
} 
//-----------------------------------------------------------------------------
// enable/disable EEPROM fixed time programming (2*Tprog)
INLINE void eeprom_fixed_time_program(uint8_t enable)
{
  if (enable) SET_BIT(FLASH->PECR, FLASH_PECR_FTDW);
  else      CLEAR_BIT(FLASH->PECR, FLASH_PECR_FTDW);
}
//-----------------------------------------------------------------------------
// read EEPROM
INLINE void eeprom_read(uint8_t *data, uint32_t address, uint32_t count)
{
  memcpy((void*) data, (const void*) address, (size_t) count);
}
//-----------------------------------------------------------------------------
// wait BUSY flag with timeout
INLINE int8_t eeprom_wait_busy(uint32_t timeout)
{
  uint32_t start = bsp_systicks;
  while (READ_BIT(FLASH->SR, FLASH_FLAG_BSY))
  {
    __WFI();
    if (bsp_systicks - start >= timeout)
      return -1; // wait BSY timeout
  }
  return 0; // success
}
//-----------------------------------------------------------------------------
// fake erase EEPROM page (fill by 0xFFFFFFFF)
// (return: 0-sucsess, -1-BUSY #1 timeout, -2-BUSY #2 timeout, -3-locked)
int8_t eeprom_erase(uint32_t address, uint32_t page_num, uint32_t timeout);
//-----------------------------------------------------------------------------
// write data to EEPROM
// (return: 0-sucsess, -1-BUSY #1 timeout, -2-BUSY #2 timeout, -3-locked)
int8_t eeprom_write(uint32_t address, const uint8_t  *data, uint32_t count,
                    uint32_t timeout);
//-----------------------------------------------------------------------------
#ifdef __cplusplus
}
#endif // __cplusplus
//-----------------------------------------------------------------------------
#endif // EEPROM_H

/*** end of "eeprom.h" file ***/

