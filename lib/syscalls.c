/*
 * Redefine newlib system calls
 * file: "syscalls.c"
 */

//-----------------------------------------------------------------------------
#include <errno.h>      // errno, EBADF
#include <sys/unistd.h> // STDOUT_FILENO, STDERR_FILENO
//-----------------------------------------------------------------------------
#include "print.h"
#include "bsp.h"
//-----------------------------------------------------------------------------
#if defined(PRINT_SWO) && defined(STM32_BIG)
_ssize_t _write(int fd, const void *data, size_t len)
{
  const char *ptr = (const char *) data;
  int i;

  if ((fd != STDOUT_FILENO) && (fd != STDERR_FILENO))
  {
    errno = EBADF;
    return -1;
  }

  for (i = 0; i < len; i++)
    ITM_SendChar((uint32_t) *ptr++);

  return len;
}
//-----------------------------------------------------------------------------
#elif defined(PRINT_USART) && defined(STM32_BIG)
_ssize_t _write(int fd, const void *data, size_t len)
{
  if ((fd != STDOUT_FILENO) && (fd != STDERR_FILENO))
  {
    errno = EBADF;
    return -1;
  }
  return bsp_usart_tx((const uint8_t*) data, len, PRINT_TIMEOUT);
}
//-----------------------------------------------------------------------------
#elif defined(PRINT_USB) && defined(STM32_BIG)
_ssize_t _write(int fd, const void *data, size_t len)
{
  if ((fd != STDOUT_FILENO) && (fd != STDERR_FILENO))
  {
    errno = EBADF;
    return -1;
  }

  len = bsp_usbcdc_tx((const uint8_t*) data, len, PRINT_TIMEOUT);

  return len;
}
#endif
//-----------------------------------------------------------------------------

/*** end of "syscalls.c" file ***/

