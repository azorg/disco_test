TFS - Trivial FLASH (File) System
=================================

Модуль tfs.c/tfs.h реализует простейшую "файловую" систему 
в области FLASH микроконтроллера STM32, в которой может храниться
один файл :-). Если требуется реализовать сохранение нескольких «файлов»,
то возможно создание нескольких объектов типа `tfs_t`.
Для стирания/записи FLASH используется модуль flash.c/flash.h.
Стирание FLASH памяти производится страницами по
FLASH_PAGE_SIZE байт.
Для работы с одной областью FLASH используется одна структура `tfs_t`.
В одну область FLASH можно записать ТОЛЬКО ОДИН файл (запись).
Структура `tfs_t` заполняется функцией tfs_init() и далее
не модифицируется.
Размер записи (условного файла) в области FLASH не может превышать
размера (page_size - 8) байт, указываемого при вызове функции tfs_init().
Размер страницы должен быть кратен величине FLASH_PAGE_SIZE (~1024).
При этом 8 байт расходуется на заголовок записи, состоящей из 4-х 16-ти
битных слов: сигнатура (sig), счётчик перезаписи файла (cnt), размера
записи (size) и контрольной суммы (cs).
В модуле реализованы _надежные_ для хранения данных и _бережливые_ для
операций со FLASH памятью алгоритмы.
Под надежностью подразумевается максимально безотказная работа устройства
в случае непредвиденного отключения питания во время операции записи
во FLASH память.
Под бережливостью понимается минимизация операций записи и стирания
FLASH памяти. При наличии свободного места новая запись записывается
в свободную область, а старая помечается как удаленная.
Может использоваться одно страничная или двух страничная организация
(см. параметр num_pages передаваемый в функцию tfs_init()).
Двух страничная организация может быть надежнее так как при стирании и
записи одной страници, во второй странице остаются предыдущие "удаленные"
резервные копии записи.
Для каждой записи может указываться свой размер:
от 0 до (page_size - 8) байт, функция чтения возвращает данный размер.
Для большей достоверности целостности данных во FLASH для каждой записи
используется простейшая контрольная сумма "ADD/XOR".

В модуле реализованы следующие пользовательские функции:

* tfs_init() - инициализация структуры `tfs_t` для работы c областью
               FLASH памяти

* tfs_get() - получить указатель на данные записи по FLASH
              памяти для чтения

* tfs_read() - прочитать запись из FLASH в оперативную память

* tfs_write() - записать новую запись во FLASH память

* tfs_delete() - принудительно пометить запись как удаленную

* tfs_erase() - принудительная очистка области FLASH

При обычной работе полагается, что основными функциями будут:
tfs_init(), tfs_get()/tfs_read и tfs_write().

Функции tfs_delete() и tfs_erase() - необязательные.

Разблокировка FLASH и последующая блокировка производятся в функциях
tfs_write(), tfs_delete() и tfs_erase().

TFS и EEPROM
------------
Изначально модуль позиционировался для использования в устройствах без
EEPROM. Было бы удобно использовать один и тот же API вне зависимости
от модели микроконтролера (есть у него EEPROM или только FLASH).
Кроме того, учитывая сведения, что EEPROM в некоторых микроконтроллерах
имеет ресурc по числу циклов чтения/записи не более, чем FLASH, 
компонент TFS был унифицирован для работы и с EEPROM памятью STM32L1xx.
Для использования EEPROM необходимо определить TFS_EEPROM.
В случае выбора EEPROM используется модуль "eeprom.h/eeprom.c"
за место мдуля "flash.h/flash.c".
Для STM32L151xx испытан вариает с EEPROM.

STM32L4/L4+
-----------
При работе с STM32L476xx обнаружилось, что работа с FLASH организована
несколько иначе по сравнению с STM32F103xx.
Запись во FLASH возможна только двойными словами по 64 бита.
Пришлось наложить соответсвующие патчи.

