/*
 * file "print.h"
 */

#ifndef PRINT_H
#define PRINT_H
//-----------------------------------------------------------------------------
//#define PRINT_SWO
//#define PRINT_USART USART1
//#define PRINT_USB
//-----------------------------------------------------------------------------
#if defined(PRINT_SWO)
#  include "bsp.h"
#elif defined(PRINT_USART)
#  include "usart.h"
#elif defined(PRINT_USB)
#  include "usbd_cdc_if.h"
#endif
#ifndef PRINT_TIMEOUT
#  define PRINT_TIMEOUT 1000 // ms
#endif
//-----------------------------------------------------------------------------
#include <stdio.h> // printf(), fprintf()
#define ERR(fmt, arg...) fprintf(stderr, "ERR: " fmt "\r\n", ## arg)
#ifdef DEBUG
#    define DBG(fmt, arg...) printf("DBG: " fmt "\r\n", ## arg)
#else
#  define DBG(fmt, ...) // debug output off
#endif // DEBUG
//-----------------------------------------------------------------------------
#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus
//-----------------------------------------------------------------------------
// print string
int print_str(const char *msg);
//-----------------------------------------------------------------------------
// print char
int print_chr(char c);
//-----------------------------------------------------------------------------
// print integer
int print_int(int i);
//-----------------------------------------------------------------------------
// hex print unsigned integer
int print_hex(unsigned int i, char digits);
//-----------------------------------------------------------------------------
#ifdef __cplusplus
}
#endif // __cplusplus
//-----------------------------------------------------------------------------
#endif // PRINT_H

/*** end of "print.h" file ***/

