/*
 * Redefine newlib system calls
 * file: "syscalls.h"
 */

#ifndef SYSCALLS_H
#define SYSVALLS_H
//-----------------------------------------------------------------------------
#include <sys/unistd.h>
#include <unistd.h>
//-----------------------------------------------------------------------------
#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus
//-----------------------------------------------------------------------------
_ssize_t _write(int fd, const void *data, size_t len);
//-----------------------------------------------------------------------------
#ifdef __cplusplus
}
#endif // __cplusplus
//-----------------------------------------------------------------------------
#endif // SYSCALLS_H

/*** end of "syscalls.h" file ***/

