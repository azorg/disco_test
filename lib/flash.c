/*
 * STM32 FLASH wrappers
 * file "flash.h"
 */

//-----------------------------------------------------------------------------
#include "flash.h"
//#include <stdio.h>
//-----------------------------------------------------------------------------
// erase FLASH page
// (return: 0-sucsess, -1-BSY timeout, -2-EOP timeout, -3-locked)
int8_t flash_erase(uint32_t address, uint32_t page_num,
                   uint32_t timeout)
{
  uint32_t i, start;

  // check FLASH unlocked
  if (READ_BIT(FLASH->CR, FLASH_CR_LOCK))
    return -3; // FLASH locked
  
  // FIXME: debug print 
  //fprintf(stderr, "FLASH erase: addr=0x%X page_num=%u\r\n",
  //        (unsigned) address, (unsigned) page_num);

#if defined(STM32L4xx)
  address = address / FLASH_PAGE_SIZE; // page index

  start = bsp_systicks;
  while (READ_BIT(FLASH->SR, FLASH_SR_BSY)) // test BSY bit
    if (bsp_systicks - start >= timeout)
      return -1; // wait BSY timeout

  SET_BIT(FLASH->SR, FLASH_SR_PGSERR); // reset PGSERR bit by write 1

  for (i = 0; i < page_num; i++)
  {
#if defined(FLASH_CR_BKER) // STM32L4x6
    // write PER (Page Erase), PNB (Page Number), BKER (bank 0/1) to FLASH->CR
    MODIFY_REG(FLASH->CR,
	       FLASH_CR_PER | FLASH_CR_PNB | FLASH_CR_BKER,
	       FLASH_CR_PER | ((address & 0xFF) << FLASH_CR_PNB_Pos) |
               (FLASH_BANK ? FLASH_CR_BKER : 0));
#else // STM32L43x
    // write PER (Page Erase), PNB (Page Number) to FLASH->CR
    MODIFY_REG(FLASH->CR,
	       FLASH_CR_PER | FLASH_CR_PNB,
	       FLASH_CR_PER | ((address & 0xFF) << FLASH_CR_PNB_Pos));
#endif

    SET_BIT(FLASH->CR, FLASH_CR_STRT); // set STRT bit (Start)
    
    start = bsp_systicks;
    while (READ_BIT(FLASH->SR, FLASH_SR_BSY)) // test BSY bit
      if (bsp_systicks - start >= timeout)
      {
        CLEAR_BIT(FLASH->CR, FLASH_CR_PER); // reset PER bit
	return -2; // wait BSY timeout
      }

    CLEAR_BIT(FLASH->CR, FLASH_CR_PER | FLASH_CR_PNB); // reset PER and PNB

    address++;
  }

#elif defined(STM32F1xx)
  address &= ~(FLASH_PAGE_SIZE - 1);

  for (i = 0; i < page_num; i++)
  {
    start = bsp_systicks;
    while (READ_BIT(FLASH->SR, FLASH_SR_BSY)) // test BSY bit
      if (bsp_systicks - start >= timeout)
        return -1; // wait BSY timeout

    if (READ_BIT(FLASH->SR, FLASH_SR_EOP))
      WRITE_REG(FLASH->SR, FLASH_SR_EOP); // reset EOP (End Operation) bit by write 1

    SET_BIT(FLASH->CR, FLASH_CR_PER);  // set PER bit (Page Erase)
    WRITE_REG(FLASH->AR, address);     // write page address
    SET_BIT(FLASH->CR, FLASH_CR_STRT); // set STRT bit (Start)

    start = bsp_systicks;
    while (!READ_BIT(FLASH->SR, FLASH_SR_EOP)) // test EOP bit
      if (bsp_systicks - start >= timeout)
      {
        CLEAR_BIT(FLASH->CR, FLASH_CR_PER); // reset PER bit
        return -2; // wait EOP timeout
      }

    WRITE_REG(FLASH->SR, FLASH_SR_EOP); // reset EOP bit by write 1
    CLEAR_BIT(FLASH->CR, FLASH_CR_PER); // reset PER bit
    
    address += FLASH_PAGE_SIZE;
  }
#endif

  return 0; // success
}
//-----------------------------------------------------------------------------
// write data to FLASH
// (return: 0-sucsess, -1-BSY timeout, -2-EOP timeout, -3-locked)
int8_t flash_write(uint32_t address, const uint8_t *data, uint32_t count,
                   uint32_t timeout)
{
  uint32_t i, start;
#if defined(STM32L4xx)
  uint32_t *ptr = (uint32_t*) data;
#elif defined(STM32F1xx)
  uint16_t *ptr = (uint16_t*) data;
#endif

  // check FLASH unlocked
  if (READ_BIT(FLASH->CR, FLASH_CR_LOCK))
    return -3; // FLASH locked

  start = bsp_systicks;
  while (READ_BIT(FLASH->SR, FLASH_SR_BSY)) // test BSY bit
    if (bsp_systicks - start >= timeout)
      return -1; // wait BSY timeout

  // FIXME: debug print 
  //fprintf(stderr, "FLASH write: addr=0x%X size=%u\r\n",
  //        (unsigned) address, (unsigned) count);

#if defined(STM32L4xx)
  SET_BIT(FLASH->SR, FLASH_SR_PGSERR); // reset PGSERR bit by write 1

  //CLEAR_BIT(FLASH->ACR, FLASH_ACR_DCEN); // disable FLASH cache FIXME

  SET_BIT(FLASH->CR, FLASH_CR_PG); // set PG bit

  // write 64-bit words
  count = (count + 7) & ~7;
  for (i = 0; i < count; i += 8)
  {
    *((__IO uint32_t*) address) = *ptr++;
    address += 4;
    
    *((__IO uint32_t*) address) = *ptr++;
    address += 4;
    
    start = bsp_systicks;
    while (READ_BIT(FLASH->SR, FLASH_SR_BSY)) // test BSY bit
      if (bsp_systicks - start >= timeout)
      {
        CLEAR_BIT(FLASH->CR, FLASH_CR_PG);
        return -2; // wait EOP timeout
      }
  }

#elif defined(STM32F1xx)
  if (READ_BIT(FLASH->SR, FLASH_SR_EOP))
    WRITE_REG(FLASH->SR, FLASH_SR_EOP); // reset EOP bit
  
  SET_BIT(FLASH->CR, FLASH_CR_PG); // set PG bit

  // write 16-bit words
  for (i = 0; i < count; i += 2)
  {
    *((__IO uint16_t*) address) = *ptr++;
    address += 2;

    start = bsp_systicks;
    while (!READ_BIT(FLASH->SR, FLASH_SR_EOP)) // test EOP bit
      if (bsp_systicks - start >= timeout)
      {
        CLEAR_BIT(FLASH->CR, FLASH_CR_PG);
        return -2; // wait EOP timeout
      }

    WRITE_REG(FLASH->SR, FLASH_SR_EOP); // reset EOP bit
  }
#endif

  CLEAR_BIT(FLASH->CR, FLASH_CR_PG); // clear PG bit

  return 0; // success
}
//-----------------------------------------------------------------------------

/*** end of "flash.c" file ***/

