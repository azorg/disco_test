/*
 * STM32 EEPROM wrappers
 * file "eeprom.c"
 */

//-----------------------------------------------------------------------------
#include "eeprom.h"
//-----------------------------------------------------------------------------
#if defined(STM32L100xB) || defined(STM32L151xB) || defined(STM32L152xB)
#  error("You STM32L1x device is only 1 Cat.; sorry.")
#endif
//-----------------------------------------------------------------------------
// fake erase EEPROM page (fill by 0xFFFFFFFF)
// (return: 0-sucsess, -1-BUSY #1 timeout, -2-BUSY #2 timeout, -3-locked)
int8_t eeprom_erase(uint32_t address, uint32_t page_num, uint32_t timeout)
{
  uint32_t cnt = page_num * (EEPROM_PAGE_SIZE / sizeof(uint32_t));

  if (eeprom_locked()) return -3; // EEPROM locked
  if (eeprom_wait_busy(timeout)) return -1; // wait BUSY #1 timeout

  for (; cnt != 0; cnt--)
  {
    *((__IO uint32_t *) address) = 0xFFFFFFFF;
    address += sizeof(uint32_t);
    if (eeprom_wait_busy(timeout)) return -2; // wait BUSY #2 timeout
  }

  return 0; // success
}
//-----------------------------------------------------------------------------
// write data to EEPROM
// (return: 0-sucsess, -1-BUSY #1 timeout, -2-BUSY #2 timeout, -3-locked)
int8_t eeprom_write(uint32_t address, const uint8_t  *data, uint32_t count,
                    uint32_t timeout)
{
  int tail = count & (sizeof(uint32_t) - 1);

  if (eeprom_locked()) return -3; // EEPROM locked
  if (eeprom_wait_busy(timeout)) return -1; // wait BUSY #1 timeout

  for (count /= sizeof(uint32_t); count != 0; count--)
  {
    *((__IO uint32_t *) address) = *((uint32_t*) data);
    address += sizeof(uint32_t);
    data    += sizeof(uint32_t);
    if (eeprom_wait_busy(timeout)) return -2; // wait BUSY #2 timeout
  }

  if (tail)
  {
    if (tail == 1)      // write last byte
      *((__IO uint8_t *) address) = *((uint8_t*) data);
    else if (tail == 2) // write last half word
      *((__IO uint16_t *) address) = *((uint16_t*) data);
    else if (tail == 3) // write last three bytes
    {
      *((__IO uint8_t  *) address++) = *((uint8_t *) data++);
      *((__IO uint16_t *) address)   = *((uint16_t*) data);
    }
    if (eeprom_wait_busy(timeout)) return -2; // wait BUSY #2 timeout
  }

  return 0; // success
}
//-----------------------------------------------------------------------------

/*** end of "eeprom.c" file ***/

