/*
 * Board Support Package functions (BSP)
 * file "bsp.h"
 */

#ifndef BSP_H
#define BSP_H
//-----------------------------------------------------------------------------
#include "tim.h"
#include "spi.h"
//-----------------------------------------------------------------------------
//#define BSP_USE_DWT
//#define BSP_USE_LED
//#define BSP_USE_USBCDC
//-----------------------------------------------------------------------------
#ifndef BSP_SPI
#  define BSP_SPI SPI1
#endif
//-----------------------------------------------------------------------------
#ifndef BSP_USART
#  define BSP_USART USART1
#endif
//-----------------------------------------------------------------------------
#ifndef BSP_TIM
#  define BSP_TIM TIM2
#endif
//-----------------------------------------------------------------------------
#if defined(STM32L011xx)
#  include "stm32l0xx.h"
#elif defined(STM32L151xC) || defined(STM32L100xC)
#  include "stm32l1xx.h"
#elif defined(STM32L432xx) || defined(STM32L476xx)
#  include "stm32l4xx.h"
#elif defined(STM32F051x8)
#  include "stm32f0xx.h"
#elif defined(STM32F103xE) || defined(STM32F103xB)
#  include "stm32f1xx.h"
#elif defined(STM32F407xx)
#  include "stm32f4xx.h"
#endif
//-----------------------------------------------------------------------------
#ifndef INLINE
#  define INLINE static inline
#endif
//-----------------------------------------------------------------------------
#ifdef BSP_USE_DWT
#  define BSP_DWT_ENABLE() \
          CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk; \
          DWT->CTRL        |= DWT_CTRL_CYCCNTENA_Msk
#  define BSP_DWT_CYCCNT DWT->CYCCNT
#endif
//-----------------------------------------------------------------------------
// global variables
extern volatile uint32_t bsp_systicks; // SysTick counter
extern volatile uint32_t bsp_timticks; // timer counter
extern volatile uint32_t bsp_rtcsec;   // RTC second counter
extern volatile uint32_t bsp_systicks_dt;  // delta SyTick counter on RTC
extern volatile uint32_t bsp_systicks_rtc; // SysTick counter on RTC wakeap
extern volatile uint32_t bsp_timticks_rtc; // timer counter on RTC wakeup

#ifdef BSP_USART
#  ifdef BSP_USART_RX_BUF
extern volatile uint32_t bsp_usart_rx_overflow;
#  endif // BSP_USART_RX_BUF
extern volatile uint32_t bsp_usart_errors;
#endif // BSP_USART

#ifdef BSP_USE_USBCDC
extern volatile uint32_t bsp_usbcdc_rx_overflow;
#endif // BSP_USE_USBCDC
//-----------------------------------------------------------------------------
#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus
//-----------------------------------------------------------------------------
// init BSP
void bsp_init();
//-----------------------------------------------------------------------------
#ifdef BSP_USE_DWT
// get ticks (DWT->SYCCNT)
INLINE uint32_t bsp_dwt_ticks() { return BSP_DWT_CYCCNT; }
#endif
//-----------------------------------------------------------------------------
// get system timer ticks (1 kHz)
INLINE uint32_t bsp_sys_ticks() { return bsp_systicks; }
//-----------------------------------------------------------------------------
// get timer ticks (~100 Hz)
INLINE uint32_t bsp_tim_ticks() { return bsp_timticks; }
//-----------------------------------------------------------------------------
// get RTC second counter (1 Hz)
INLINE uint32_t bsp_rtc_sec() { return bsp_rtcsec; }
//-----------------------------------------------------------------------------
// go to STANDBY mode
void bsp_standby_mode();
//-----------------------------------------------------------------------------
// go to STOP mode (deep sleep)
void bsp_stop_mode();
//-----------------------------------------------------------------------------
// go to FAKE STOP mode (wait RTC interrupt for debug)
void bsp_fake_stop_mode();
//-----------------------------------------------------------------------------
// go to RUN mode
void bsp_run_mode();
//-----------------------------------------------------------------------------
// go to SLEEP mode (wait for any interrupt)
void bsp_sleep_mode();
//-----------------------------------------------------------------------------
// go to Low-Power SLEEP mode (wait for any interrupt)
void bsp_lpsleep_mode();
//-----------------------------------------------------------------------------
// go to SLEEP mode for some milliseconds (wait system timer interrupt)
void bsp_sleep(uint32_t ms);
//-----------------------------------------------------------------------------
// loop-delay based on System Timer (do not sleep)
void bsp_delay(uint32_t ms);
//-----------------------------------------------------------------------------
#ifdef BSP_USE_DWT
// loop-delay based on DWT_SYSCNT
void bsp_delay_us(uint32_t us);
#endif
//-----------------------------------------------------------------------------
// on/off onboard LED
void bsp_led(int on);
void bsp_led2(int on);
//-----------------------------------------------------------------------------
// blink onboard LED
void bsp_blink_led();
//-----------------------------------------------------------------------------
#ifdef BSP_SPI
// SPI exchange wrapper function
// return:
//    0 - on error (timeout)
//    1 - on success
uint8_t bsp_spi_exchange(
  uint8_t       *rx_buf,  // RX buffer
  const uint8_t *tx_buf,  // TX buffer
  uint32_t      size,     // number of bytes
  uint32_t      timeout); // timeout [ms]
#endif // BSP_SPI
//-----------------------------------------------------------------------------
#ifdef BSP_USART
// TX data to USART
// (return number of sent bytes)
uint32_t bsp_usart_tx(
  const uint8_t *data, // TX data
  uint32_t size,       // number of bytes
  uint32_t timeout);   // timeout [ms]
//-----------------------------------------------------------------------------
// check TX USART buffer status
// (return number of bytes in TX buffer)
uint32_t bsp_usart_tx_status();
//-----------------------------------------------------------------------------
// flush TX USART buffer
// (return number bytes in TX buffer at the end of timeout)
uint32_t bsp_usart_tx_flush(uint32_t timeout); // timeout [ms]
//-----------------------------------------------------------------------------
// RX data from USART
// (return number of received bytes 0...size)
uint32_t bsp_usart_rx(
  uint8_t *data,     // RX data buffer
  uint32_t size,     // size of RX buffer
  uint32_t timeout); // timeout [ms]
//-----------------------------------------------------------------------------
// check RX USART buffer status
// (return number of bytes in RX buffer)
uint32_t bsp_usart_rx_status();
//-----------------------------------------------------------------------------
// USART callback (called from "stm32f1xx_it.c")
void bsp_usart_callback();
#endif // BSP_USART
//-----------------------------------------------------------------------------
#ifdef BSP_USE_USBCDC
// TX data to USB-CDC (write to buffer)
// (return number of sent bytes)
uint32_t bsp_usbcdc_tx(
  const uint8_t *data, // TX data
  uint32_t size,       // number of bytes
  uint32_t timeout);   // timeout [ms]
//-----------------------------------------------------------------------------
// check TX USB-CDC buffer status
// (return number of bytes in TX buffer)
uint32_t bsp_usbcdc_tx_status();
//-----------------------------------------------------------------------------
// flush TX data from output USB-CDC buffer 
// (return USBD_OK, USBD_BUSY or error code)
uint8_t bsp_usbcdc_tx_flush(uint32_t timeout); // timeout ms
//-----------------------------------------------------------------------------
// RX data from USB-CDC
// (return number of received bytes 0...size)
uint32_t bsp_usbcdc_rx(
  uint8_t *data,     // RX data buffer
  uint32_t size,     // size of RX buffer
  uint32_t timeout); // timeout [ms]
//-----------------------------------------------------------------------------
// check RX USB-CDC buffer status
// (return number of bytes in RX buffer)
uint32_t bsp_usbcdc_rx_status();
//-----------------------------------------------------------------------------
// USB-CDC callback (called from "usbd_cdc_if.c")
// (return received/saved bytes)
uint32_t bsp_usbcdc_rx_callback(uint8_t *buf, uint32_t size);
#endif // BSP_USE_USBCDC
//-----------------------------------------------------------------------------
// SysTick timer callback 1kHz (called from "stm32xxxx_it.c")
void bsp_systick_callback();
//-----------------------------------------------------------------------------
#ifdef BSP_TIM
// timer callback (called from "stm32xxxx_it.c")
void bsp_timer_callback();
//-----------------------------------------------------------------------------
// user timer callback (called from bsp_timer_callback())
void timer_callback();
#endif // BSP_TIM
//-----------------------------------------------------------------------------
// RTC callback 1Hz (called from "stm32xxxx_it.c")
void bsp_rtc_callback();
//-----------------------------------------------------------------------------
#ifdef __cplusplus
}
#endif // __cplusplus
//-----------------------------------------------------------------------------
#endif // BSP_H

/*** end of "bsp.h" file ***/

