/*
 * TFS - trivial FLASH/EEPROM "file" system
 * file: "tfs.c"
 */

//-----------------------------------------------------------------------------
#include "tfs.h"
#include <stdlib.h>
#include <string.h>
//-----------------------------------------------------------------------------
#ifdef TFS_EEPROM // use EEPROM on STM32L1x
#  include "eeprom.h"
#  define TFS_MEMTYPE               "EEPROM"
#  define TFS_PAGE_SIZE             EEPROM_PAGE_SIZE
#  define TFS_LOCK()                eeprom_lock()
#  define TFS_UNLOCK()              eeprom_unlock()
#  define TFS_ERASE(addr, n, to)    eeprom_erase(addr, n, to)
#  define TFS_WRITE(addr, d, c, to) eeprom_write(addr, d, c, to)
#else // use FLASH on STM32F1xx/STM32L4xx
#  include "flash.h"
#  define TFS_MEMTYPE               "FLASH"
#  define TFS_PAGE_SIZE             FLASH_PAGE_SIZE
#  define TFS_LOCK()                flash_lock()
#  define TFS_UNLOCK()              flash_unlock()
#  define TFS_ERASE(addr, n, to)    flash_erase(addr, n, to)
#  define TFS_WRITE(addr, d, c, to) flash_write(addr, d, c, to)
#endif
//-----------------------------------------------------------------------------
#ifdef TFS_DEBUG
#  include <stdio.h>
#  define TFS_DBG(fmt, arg...) printf("TFS: " fmt "\r\n", ## arg)
#else
#  define TFS_DBG(fmt, ...) // debug output off
#endif // TFS_DEBUG
//-----------------------------------------------------------------------------
// record header format
// ^^^^^^^^^^^^^^^^^^^^
// offset | bytes | ident   | value
//--------+-------+---------+--------------------------------------------------
//  0     | 2(10) | sig     | signature:
//        |       | FREE    | 0xFFFF - free space
//        |       | DATA    | 0x55AA - data record
//        |       | DELETED | 0x0000 - delete record
//--------+-------+---------+--------------------------------------------------
//  2108) |  2    | cnt     | (re)write counter (start value is 0)
//--------+-------+---------+--------------------------------------------------
//  4(12) |  2    | size    | Size of record data [0x0000..0xFFFF]
//--------+-------+---------+--------------------------------------------------
//  6(14) |  2    | cs      | Check Sum (ADD and XOR)
//--------+-------+---------+--------------------------------------------------
//  8(16) | size  | data    | record data (size bytes)
//--------+-------+---------+--------------------------------------------------
//
// page states:
// ^^^^^^^^^^^^
//  0. Empty  - initial state after erase FLASH/EEPROM (all words are 0xFFFF)
//  1. Opened - first sig=0x55AA or sig=0x0000
//
//  If two page used then may be next combination:
//    Empty  and Empty  - initial state after tfs_erase()
//    Opened and Empty  - write to first page
//    Empty  and Opened - write to secnd page
//    Opened and Opened - error state
//
//-----------------------------------------------------------------------------
// record signatures (16-bit words):
#define TFS_SIG_FREE    0xFFFF // free space
#define TFS_SIG_DATA    0x55AA // data record
#define TFS_SIG_DELETED 0x0000 // deleted record
//-----------------------------------------------------------------------------
#if defined(STM32L4xx)
#  define TFS_HDR_SIZE 8 // header size (16 bit words)
#  define TFS_OFF_SIG  0 // signature offset
#  define TFS_OFF_CNT  5 // (re)write counter offset
#  define TFS_OFF_SIZE 6 // size offset
#  define TFS_OFF_CS   7 // check sum offset
#  define TFS_ALIGN    4 // alignment
#else // STM32F1xx
#  define TFS_HDR_SIZE 4 // header size (16-bit words)
#  define TFS_OFF_SIG  0 // signature offset
#  define TFS_OFF_CNT  1 // (re)write counter offset
#  define TFS_OFF_SIZE 2 // size offset
#  define TFS_OFF_CS   3 // check sum offset
#  define TFS_ALIGN    1 // alignment
#endif
//-----------------------------------------------------------------------------
// alignment record size (bytes)
#define TFS_ALIGN_SIZE(size) \
        ((size + (TFS_ALIGN * 2 - 1)) & ~(TFS_ALIGN * 2 - 1))

// alignment offset (16-bit words)
#define TFS_ALIGN_OFF(size) (TFS_ALIGN_SIZE(size) >> 1)
//-----------------------------------------------------------------------------
#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus
//-----------------------------------------------------------------------------
// calculate trivial ADD/XOR check sum
static uint16_t tfs_cs(const void *data, uint16_t size)
{
  const uint8_t *ptr = (const uint8_t*) data;
  uint8_t add = 0x55, xor = 0xAA;
  while (size--)
  {
    uint8_t byte = *ptr++;
    add += byte;
    xor ^= byte;
  }
  return (((uint16_t) add) << 8) | ((uint16_t) xor);
}
//-----------------------------------------------------------------------------
// find pointer to header of record in FLASH/EEPROM region and page index
// access mode:
//   0 - get any record
//   1 - get only non deleted record
//   2 - get only deleted record
// return: TFS_ERR_SUCCESS or TFS_ERR_NOTFOUND or TFS_ERR_FORMAT
static uint16_t tfs_hdr(const tfs_t *self, uint8_t mode,
                        uint16_t **hdr, uint8_t *page)
{
  uint16_t *address   = (uint16_t*) self->address;
  uint32_t offset_max = self->page_size / sizeof(uint16_t) - TFS_HDR_SIZE;
  uint16_t pg;
  uint8_t  result = 0; // 0-not found, 1-found
    
  for (pg = 0; pg <= self->max_page; pg++)
  {
    uint32_t offset = 0;
    do {
      uint16_t *ptr = address + offset;
      if (ptr[TFS_OFF_SIG] == TFS_SIG_FREE)
      { // end of used FLASH/EEPROM area in page (or empty page)
        break;
      }
      else if (ptr[TFS_OFF_SIG] == TFS_SIG_DATA)
      { // find non deleted record
        if (mode != 2)
        {
          *hdr   = ptr;
          *page  = pg;
          return TFS_SUCCESS;
        }
      }
      else if (ptr[TFS_OFF_SIG] == TFS_SIG_DELETED)
      { // find deleted record
        if (mode != 1)
        {
          result = 1;
          *hdr   = ptr;
          *page  = pg;
        }
      }
      else
      { // unknown (bad) signature
        *hdr  = (uint16_t*) NULL;
        *page = pg;
        return TFS_ERR_FORMAT;
      }
      offset += TFS_ALIGN_OFF(ptr[TFS_OFF_SIZE]) + TFS_HDR_SIZE;
    } while (offset < offset_max);
    address += self->page_size / sizeof(uint16_t);
  } // for (pg..

  if (result == 0)
  { // record not found
    *hdr  = (uint16_t*) NULL;
    *page = 0;
    return TFS_ERR_NOTFOUND;
  }

  return TFS_ERR_DELETED;
}
//-----------------------------------------------------------------------------
// find pointer to maximal free space (>=size) in opened page(s)
// return: TFS_SUCCESS or TFS_ERR_NOSPACE or TFS_ERR_FORMAT
static uint16_t tfs_find_free_space(const tfs_t *self, uint16_t size,
                                    uint16_t **hdr, uint8_t *page)
{
  uint16_t *address   = (uint16_t*) self->address;
  uint32_t offset_max = self->page_size / sizeof(uint16_t) - TFS_HDR_SIZE;
  uint16_t max_size = 0;
  uint8_t pg, result = 0; // 0-not found, 1-found

  for (pg = 0; pg <= self->max_page; pg++)
  {
    uint16_t *ptr = address;
    if (ptr[TFS_OFF_SIG] != TFS_SIG_FREE)
    { // page not empty
      uint32_t offset = 0;
      do {
        ptr = address + offset;
        if (ptr[TFS_OFF_SIG] == TFS_SIG_FREE)
        { // end of used FLASH/EEPROM area in page
          uint16_t free_space = self->page_size -
                                (offset + TFS_HDR_SIZE) * sizeof(uint16_t);
          TFS_DBG("free_space=%u on page %i",
                  (unsigned) free_space, (int) pg);
          if (size <= free_space && 
              (result == 0 || max_size < free_space))
          { // free space size enougth
            result = 1;
            max_size = free_space;
            *hdr  = ptr;
            *page = pg;
          }
          break;
        }
        else if (ptr[TFS_OFF_SIG] != TFS_SIG_DATA &&
                 ptr[TFS_OFF_SIG] != TFS_SIG_DELETED)
        { // unknown (bad) signature
          *hdr  = (uint16_t*) NULL;
          *page = pg;
          return TFS_ERR_FORMAT;
        }
        offset += TFS_ALIGN_OFF(ptr[TFS_OFF_SIZE]) + TFS_HDR_SIZE;
      } while (offset < offset_max);
    } // if (..
    address += self->page_size / sizeof(uint16_t);
  } // for (pg..
  
  if (result == 0)
  { // free space not found
    *hdr  = (uint16_t*) NULL;
    *page = 0;
    return TFS_ERR_NOSPACE;
  }

  return TFS_SUCCESS;
}
//-----------------------------------------------------------------------------
// find pointer to full empty page 
// return: TFS_SUCCESS or TFS_ERR_NOTFOUND
static uint16_t tfs_find_empty_page(const tfs_t *self,
                                    uint16_t **hdr, uint8_t *page)
{
  uint16_t *address = (uint16_t*) self->address;
  uint8_t pg;
  for (pg = 0; pg <= self->max_page; pg++)
  {
    uint32_t i;
    uint8_t result = 1; // 0-not found, 1-found (page empty)
    uint16_t *ptr = address;
    *hdr  = ptr;
    *page = pg;
    for (i = (self->page_size / sizeof(uint16_t)); i != 0; i--)
    {
      if (*ptr++ != TFS_SIG_FREE)
      {
        *page = result = 0;
        *hdr  = (uint16_t*) NULL;
        break;
      }
    }
    if (result)
      return TFS_SUCCESS;
    address += self->page_size / sizeof(uint16_t);
  }
  return TFS_ERR_NOTFOUND;
}
//-----------------------------------------------------------------------------
// init TFS structure (initial fill FLASH/EEPROM region descriptor)
void tfs_init(tfs_t    *self,     // FLASH/EEPROM region
              uint8_t  num_pages, // number of pages (1 or 2)
              uint32_t address,   // base FLASH/EEPROM address
              uint32_t page_size) // size of page [bytes]
{
  self->page_size = (page_size + 7) & ~7;
  self->address   = address;
  self->max_page  = num_pages <= 1 ? 0 : 1;
#ifdef TFS_EEPROM
  eeprom_fixed_time_program(0); // fast EEPROM (?!)
#endif
}
//-----------------------------------------------------------------------------
// erase FLASH/EEPROM region
// return code:
//   TFS_SUCCESS    - success write record to FLASH/EEPROM region
//   TFS_ERR_ERASE  - erase FLASH error
uint16_t tfs_erase(const tfs_t *self)
{
  uint8_t pg;
  uint32_t address = self->address;
  uint32_t n = self->page_size / TFS_PAGE_SIZE;
  uint16_t err = TFS_SUCCESS;

  TFS_DBG("unlock " TFS_MEMTYPE);
  TFS_UNLOCK(); 

  TFS_DBG("erase all (%u*%u=%u) " TFS_MEMTYPE " pages",
          (unsigned) n, (unsigned) self->max_page + 1,
          (unsigned) (n * (self->max_page + 1)));

  for (pg = 0; pg <= self->max_page; pg++)
  {
    int8_t rv = TFS_ERASE(address, n, TFS_TIMEOUT);
    if (rv)
    {
      TFS_DBG("error: can't erase page %u", (unsigned) pg);
      err = TFS_ERR_ERASE;
      goto erase_error;
    }
    address += self->page_size;
  }

erase_error:
  TFS_DBG("lock " TFS_MEMTYPE);
  TFS_LOCK();
  return err;
}
//-----------------------------------------------------------------------------
// get pointer to record data in FLASH/EEPROM region
// return error code, pointer to data in FLASH/EEPROM space and record size
// return code:
//   TFS_SUCCESS                  - find record success
//   TFS_SUCCESS     | TFS_ERR_CS - find record with bad check sum (CS)
//   TFS_ERR_DELETED              - find only old deleted rrecord
//   TFS_ERR_DELETED | TFS_ERR_CS - find only old deleted record with bad CS
//   TFS_ERR_NOTFOUND             - record not found
//   TFS_ERR_FORMAT               - corrupt TFS format (page(s) need erase) 
uint16_t tfs_get(const tfs_t *self,           // FLASH/EEPROM region
                 void **data, uint16_t *size, // record region
                 uint16_t *cnt)               // (re)write counter
{
  uint8_t page;
  uint16_t *hdr;
  uint16_t err = tfs_hdr(self, 0, &hdr, &page);


  if ((err & (~TFS_ERR_DELETED)) == TFS_SUCCESS)
  { // found record (may be deleted)
    TFS_DBG("record found (page=%i deleted=%i cnt=%u addr=0x%08X err=0x%04X)",
            page,
            (int) !!(err & TFS_ERR_DELETED),
            (unsigned) hdr[TFS_OFF_CNT],
            (unsigned) hdr,
            (unsigned) err);
  }
  else
  { // not fount any record or error
    TFS_DBG("record not found (err=0x%04X)", (unsigned) err);
    *data = (void*) NULL;
    *cnt  = 0;
    *size = 0;
    return err; // TFS_ERR_NOTFOUND or TFS_ERR_FORMAT
  }

  *data = (void*) (hdr + TFS_HDR_SIZE);
  *cnt  = hdr[TFS_OFF_CNT];
  *size = hdr[TFS_OFF_SIZE];

  if (hdr[TFS_OFF_CS] != tfs_cs(*data, *size))
  { // bad check sum 
    TFS_DBG("bad check sum");
    err |= TFS_ERR_CS;
    if (!(err & TFS_ERR_DELETED))
    { // try to read deleted record as backup
      uint8_t d_page;
      uint16_t *d_hdr, d_err;
      TFS_DBG("try to read deleted record as backup");

      d_err = tfs_hdr(self, 2, &d_hdr, &d_page);
  
      if (d_err == TFS_ERR_DELETED)
      { // found deleted record
        void *d_data = (void*) (d_hdr + TFS_HDR_SIZE);

        TFS_DBG("deleted record found (page=%i addr=0x%08X err=0x%04X)",
                d_page,
                (unsigned) d_hdr,
                (unsigned) d_err);

        if (d_hdr[TFS_OFF_CS] == tfs_cs(d_data, d_hdr[TFS_OFF_SIZE]))
        { // check sum of deleted record is OK
          TFS_DBG("check sum of deleted record is OK");
          err &= ~TFS_ERR_CS;
          err |= TFS_ERR_DELETED;
          *data = d_data;
          *cnt  = d_hdr[TFS_OFF_CNT];
          *size = d_hdr[TFS_OFF_SIZE];
        }
      }
      else
      {
        TFS_DBG("deleted record not found (err=0x%04X)", (unsigned) d_err);
      }
    }
  }

  return err;
}
//-----------------------------------------------------------------------------
// read record from FLASH/EEPROM region
// find and copy record from FLASH/EEPROM to buffer in RAM,
// return error code and record size
// return code:
//   TFS_SUCCESS                  - find record success
//   TFS_SUCCESS     | TFS_ERR_CS - find record with bad check sum (CS)
//   TFS_ERR_DELETED              - find only old deleted rrecord
//   TFS_ERR_DELETED | TFS_ERR_CS - find only old deleted record with bad CS
//   TFS_ERR_NOTFOUND             - record not found
//   TFS_ERR_FORMAT               - corrupt TFS format (page(s) need erase) 
uint16_t tfs_read(const tfs_t *self,                  // FLASH/EEPROM region
                  void *buffer, uint16_t buffer_size, // destination buffer
                  uint16_t *size,                     // size of record
                  uint16_t *cnt)                      // (re)write counter
{
  void *data;
  uint16_t err = tfs_get(self, &data, size, cnt);

  if (buffer_size > *size)
    buffer_size = *size;

  if (buffer_size)
    memcpy((void*) buffer, (const void*) data, buffer_size);

  return err;
}
//-----------------------------------------------------------------------------
// write record to FLASH/EEPROM region (mega funcion)
// return code:
//   TFS_SUCCESS    - success write record to FLASH/EEPROM region
//   TFS_ERR_TOOBIG - too big record size
//   TFS_ERR_ERASE  - erase error
//   TFS_ERR_WRITE  - write error
//   TFS_ERR_VERIFY - verify error
uint16_t tfs_write(const tfs_t *self,               // FLASH/EEPROM region
                   const void *data, uint16_t size) // record source
{
  uint16_t retv, err = TFS_SUCCESS; // return values
  int8_t rv;                        //
  uint8_t pg; // page index 0..1

  uint16_t hdr[TFS_HDR_SIZE]; // hedaer of new record
  
  uint16_t *last_hdr;      // pointer to last record header
  uint8_t   last_page = 0; // last record page index {0|1}

  uint16_t cnt    = 0;  // last record (re)write counter

  uint8_t  found  = 0;  // last record found flag {0|1} 
  uint8_t  delete = 0;  // delete last record flag {0|1}
  uint8_t  close  = 0;  // last record page close flag {0|1}

  uint8_t erase[2] = {0, 0}; // erase page flags {0|1}

  uint16_t *free_hdr = 0;  // pointer to free space in opened page
  uint8_t   free_page = 0; // free space page index {0|1}

#if defined(STM32L4xx)
  hdr[TFS_OFF_SIG + 1] = TFS_SIG_DELETED;
  hdr[TFS_OFF_SIG + 2] = TFS_SIG_DELETED;
  hdr[TFS_OFF_SIG + 3] = TFS_SIG_DELETED;
  hdr[TFS_OFF_SIG + 4] = TFS_SIG_FREE;
#endif

  if (size > (self->page_size - TFS_HDR_SIZE * sizeof(uint16_t)))
  {
    size = self->page_size - TFS_HDR_SIZE * sizeof(uint16_t); // trancate size
    err = TFS_ERR_TOOBIG;
  }

  // find last record
  retv = tfs_hdr(self, 0, &last_hdr, &last_page);
  if ((retv & ~TFS_ERR_DELETED) == TFS_SUCCESS)
  { // old record found
    found = 1;
    delete = !(retv & TFS_ERR_DELETED); // mark to delete record
    cnt = last_hdr[TFS_OFF_CNT];
    TFS_DBG("old record found (page=%i deleted=%i cnt=%u addr=0x%08X)",
            (int) last_page, (int) !delete,
            (unsigned) cnt, (unsigned) last_hdr);
    cnt++;
  }
  else if (retv == TFS_ERR_FORMAT)
  { // format error => page must be erased
    erase[0] = erase[1] = 1;
    TFS_DBG("format error => all page(s) must be erased");
  }

  // find pointer to maximal free space in opened page(s)
  TFS_DBG("find pointer to maximal free space in opened page(s)");
  retv = tfs_find_free_space(self, size, &free_hdr, &free_page);
  if (retv == TFS_ERR_NOSPACE)
  { // no free space in opened page(s) => try to find empty page
    TFS_DBG("no free space in opened page(s) => try to find empty page");
    retv = tfs_find_empty_page(self, &free_hdr, &free_page);
    if (retv == TFS_SUCCESS)
    { // empty page found
      TFS_DBG("empty page found");
      close = found;
    }
    else // retv == TFS_ERR_NOTFOUND
    { // empty page not found => erase page for write
      TFS_DBG("empty page not found => must erase page %u for write",
              (unsigned) free_page);
      free_page = (last_page ^ 1) & self->max_page;
      erase[free_page] = 1;
      free_hdr = (uint16_t*)
                 (self->address + free_page * self->page_size);
      close = free_page ^ last_page;
      delete &= close;
    }
  }
  else if (retv == TFS_ERR_FORMAT)
  { // format error => page(s) must be erased
    TFS_DBG("format error => page(s) must be erased");
    erase[0] = erase[1] = 1;
    free_hdr = (uint16_t*) self->address;
    free_page = close = delete = 0;
  }

  TFS_DBG("unlock " TFS_MEMTYPE);
  TFS_UNLOCK(); 

  if (delete)
  { // delete record
    TFS_DBG("delete record from page %i", (int) last_page);
    hdr[TFS_OFF_SIG] = TFS_SIG_DELETED;
    rv = TFS_WRITE((uint32_t) last_hdr,
                   (const uint8_t*) hdr,
                   (uint32_t) TFS_ALIGN * sizeof(uint16_t),
                   TFS_TIMEOUT);
    if (rv)
    {
      TFS_DBG("error: cant't delete record");
      err |= TFS_ERR_WRITE;
      goto unlock_exit;
    }
  }

  // erase page(s) for write
  for (pg = 0; pg <= self->max_page; pg++)
  {
    if (erase[pg])
    {
      uint32_t address = self->address + pg * self->page_size;
      uint32_t n = self->page_size / TFS_PAGE_SIZE;
      TFS_DBG("erase page %u for write", (unsigned) pg);
      rv = TFS_ERASE(address, n, TFS_TIMEOUT);
      if (rv) goto error_cant_erase;
    }
  }

  // write record
  TFS_DBG("write record to page %i (addr=0x%08X size=%u cnt=%u)",
          (int) free_page, (unsigned) free_hdr,
          (unsigned) size, (unsigned) cnt);
  hdr[TFS_OFF_SIG]  = TFS_SIG_DATA;
  hdr[TFS_OFF_CNT]  = cnt;
  hdr[TFS_OFF_SIZE] = size;
  hdr[TFS_OFF_CS]   = tfs_cs(data, size);
  rv = TFS_WRITE((uint32_t) free_hdr,
                 (const uint8_t*) hdr,
                 (uint32_t) TFS_HDR_SIZE * sizeof(uint16_t),
                 TFS_TIMEOUT);
  if (rv)
  {
    TFS_DBG("error: cant't write header (TFS_HDR_SIZE=%i)",
            (int) TFS_HDR_SIZE);
    err |= TFS_ERR_WRITE;
    goto unlock_exit;
  }

  rv = TFS_WRITE((uint32_t) (free_hdr + TFS_HDR_SIZE),
                 (const uint8_t*) data,
                 TFS_ALIGN_SIZE(size),
                 TFS_TIMEOUT);

  if (rv)
  { 
    TFS_DBG("error: cant't write data (size=%i)", (int) size);
    err |= TFS_ERR_WRITE;
    goto unlock_exit;
  }

#ifdef TFS_DEBUG
  // show free space for debug
  if (1)
  {
    int32_t free_space = 
            (int32_t) (self->address + self->page_size * (free_page + 1)) -
            (int32_t) (free_hdr + 2 * TFS_HDR_SIZE) -
	    (int32_t) TFS_ALIGN_SIZE(size);
    if (free_space > 0)
      TFS_DBG("free_space=%i on page %i", (int) free_space, (int) free_page);
    else
      TFS_DBG("page %i is full", (int) free_page);
  }
#endif
  
  // verify
  if (1)
  {
    const uint8_t *src = (const uint8_t*) data, *dst;
    uint8_t page;
    uint16_t *ptr, i;
    retv = tfs_hdr(self, 1, &ptr, &page);
    if (retv != TFS_SUCCESS)
    {
      TFS_DBG("error: cant't find record");
      err |= TFS_ERR_VERIFY;
      goto unlock_exit;
    }

    for (i = 0; i < TFS_HDR_SIZE; i++)
      if (*ptr++ != hdr[i])
      {
        TFS_DBG("error: verify header fail (ix=%i)", i);
        err |= TFS_ERR_VERIFY;
        goto unlock_exit;
      }

    dst = (const uint8_t*) ptr;
    for (i = 0; i < size; i++)
      if (*src++ != *dst++)
      {
        TFS_DBG("error: verify data fail (ix=%i)", i);
        err |= TFS_ERR_VERIFY;
        goto unlock_exit;
      }
  }

  TFS_DBG("verify success");

  if (close)
  { // erase closed page
    uint32_t address = self->address + last_page * self->page_size;
    uint32_t n = self->page_size / TFS_PAGE_SIZE;
    TFS_DBG("erase closed page %u", (unsigned) last_page);
    rv = TFS_ERASE(address, n, TFS_TIMEOUT);
    if (rv)
    {
error_cant_erase:
      TFS_DBG("error: can't erase page");
      err |= TFS_ERR_ERASE;
      goto unlock_exit;
    }
  }

  TFS_DBG("lock " TFS_MEMTYPE);
  TFS_LOCK();
  
  return err;
      
unlock_exit:
  TFS_DBG("write error (err=0x%04X)", (unsigned) err);
  TFS_DBG("lock " TFS_MEMTYPE);
  TFS_LOCK();
  return err;
}
//-----------------------------------------------------------------------------
// delete record in FLASH/EEPROM region (mark record as deleted)
// return code:
//   TFS_SUCCESS      - success write record to FLASH/EEPROM region
//   TFS_ERR_NOTFOUND - record not found
//   TFS_ERR_WRITE    - write FLASH/EEPROM error
uint16_t tfs_delete(const tfs_t *self)
{
  uint16_t *hdr;
  uint8_t  page;
  uint16_t err = tfs_hdr(self, 1, &hdr, &page);
  
  if (err == TFS_SUCCESS)
  {
    int8_t rv;
#if defined(STM32L4xx)
    static const uint16_t sig[4] = { TFS_SIG_DELETED, TFS_SIG_DELETED,
                                     TFS_SIG_DELETED, TFS_SIG_DELETED };
#else // STM32F1xx
    static const uint16_t sig[1] = { TFS_SIG_DELETED };
#endif

    TFS_DBG("unlock " TFS_MEMTYPE);
    TFS_UNLOCK();
    
    TFS_DBG("delete record from page %i", (int) page);
    rv = TFS_WRITE((uint32_t) hdr,
                   (const uint8_t*) sig,
                   (uint32_t) TFS_ALIGN * sizeof(uint16_t),
                   TFS_TIMEOUT);

    if (rv) TFS_DBG("error: cant't delete record");
    
    TFS_DBG("lock " TFS_MEMTYPE);
    TFS_LOCK();

    if (rv) return TFS_ERR_WRITE;
  }

  return err;
}
//-----------------------------------------------------------------------------

/*** end of "tfs.c" file ***/

