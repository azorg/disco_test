# Install STM32 development linux tools HOWTO

## Install gcc-arm, binutils-arm, newlib-arm and gdb-arm
```
sudo apt install gcc-arm-none-eabi
sudo apt install binutils-arm-none-eabi

sudo apt install libnewlib-doc
```

## Install STM32CubeMX (version >= 5.5.0) from http://st.com
Go to https://www.st.com/content/st_com/en/products/development-tools/software-development-tools/stm32-software-development-tools/stm32-configurators-and-code-generators/stm32cubemx.html

## Make help start file `/usr/local/bin/cubemx`
```
#!/bin/sh
java -jar /usr/local/STMicroelectronics/STM32Cube/STM32CubeMX/STM32CubeMX $*
```
or
```
#!/bin/bash
/usr/local/STMicroelectronics/STM32Cube/STM32CubeMX/STM32CubeMX $*

```

## Intall dos2unix (convert Makefile)
```
apt install dos2unix
```

## Install stlink utils from https://github.com/texane/stlink
```
sudo apt install libgtk-3-dev # GUI
git clone https://github.com/texane/stlink.git
cd stlink
mkdir build && cd build
cmake ..
make
sudo make install
```

Alternative for Debian 10 buster:
```
sudo apt install stlink-gui stlink-tools
```

# Install openocd (from source or packages)
Go to https://github.com/ntfreak/openocd

or use apt:
```
sudo apt install openocd
```

