/*
 * File: "disco_test.h"
 */

#ifndef DISCO_TEST_H
#define DISCO_TEST_H
//----------------------------------------------------------------------------
#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus
//-----------------------------------------------------------------------------
// main function of project
void disco_test();
//-----------------------------------------------------------------------------
// button callback
void button_callback();
//-----------------------------------------------------------------------------
#ifdef __cplusplus
}
#endif // __cplusplus
//----------------------------------------------------------------------------
#endif // DISCO_TEST_H

/*** end of "disco_test.h" file ***/


